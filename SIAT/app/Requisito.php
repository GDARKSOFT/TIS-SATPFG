<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requisito extends Model
{
    public $table = "requisito";
    
    public function modalidad()
    {
        return $this
            ->belongsToMany('App\Modalidad')
            ->withTimestamps();
    }

    public function funciones()
    {
        return $this
            ->belongsToMany('App\Funcion')
            ->withTimestamps();
    } 
}
