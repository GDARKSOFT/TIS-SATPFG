<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    public $table = "modalidad";
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'des_mod'];

    public function requisitos()
    {
        return $this
            ->belongsToMany('App\Requisito', 'modalidad_requisito', 'mod_id', 'req_id')
            ->withTimestamps();
    }
}
