<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcion extends Model
{
    
	public $table = "funcion";
    public function roles()
    {
        return $this
            ->belongsToMany('App\Rol')
            ->withTimestamps();
    }

}
