<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Area extends Model
{   
    public $table = "area";
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'nom_area', 'des_area', 'area_id'];

    public static function filtroSubarea($id){
        return Area::where('area_id', $id)->get();
    }  
        
    public function subareas()
    {
        return $this
            ->hasMany('App\Area', 'area_id');
    }
    
    public function area(){
        return $this->belongsTo('App\Area', 'area_id');
    }
    public function profesionales()
    {
        return $this
            ->belongsToMany('App\Profesional','profesional_competencia','prof_id','subar_id')
            ->withTimestamps();
    }
    public function profesionalesIntereses()
    {
        return $this
            ->belongsToMany('App\Profesional','profesional_interes','prof_id','subar_id')
            ->withTimestamps();
    }
    public function proyectos()
    {
        return $this
        ->belongsToMany('App\Proyecto','proyecto_area', 'proy_id', 'area_id')
        ->withTimestamps();
    }
}
