<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesional_Competencia extends Model
{
    public $table = "profesional_competencia";
    protected $primaryKey = 'subar_id';
    protected $fillable = ['prof_id','subar_id'];

}
