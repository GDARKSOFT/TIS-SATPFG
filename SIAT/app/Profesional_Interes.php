<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesional_Interes extends Model
{
    public $table = "profesional_interes";
    protected $primaryKey = 'subar_id';
    protected $fillable = ['prof_id','subar_id'];
}
