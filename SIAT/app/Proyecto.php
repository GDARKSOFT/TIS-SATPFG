<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    public $table = "proyecto";
    protected $primaryKey = 'id';
	protected $fillable = [ 'tit_proy',
                            'nom_est',
                            'app_est',
                            'apm_est',
                            'carr_id',
                            'tut_id',
                            'mod_id',
                            'est_id'];
	
	public function carrera(){
        return $this->belongsTo('App\Carrera', 'carr_id');
    }

    public function modalidad(){
        return $this->belongsTo('App\Modalidad', 'mod_id');
    }

	public function estado(){
        return $this->belongsTo('App\Estado', 'est_id');
    }

	public function profesionales(){
		return $this
            ->belongsToMany('App\Profesional', 'profesional_proyecto', 'proy_id', 'prof_id')
            ->withTimestamps();
	}

    public function areas(){
        return $this
            ->belongsToMany('App\Area', 'proyecto_area', 'proy_id', 'area_id')
            ->withTimestamps();
    }
}
