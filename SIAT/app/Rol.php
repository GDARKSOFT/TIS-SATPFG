<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rol extends Model
{
    public $table = "rol";
    public function users()
    {
        return $this
            ->belongsToMany('App\User')
            ->withTimestamps();
    }

    public function funciones()
    {
        return $this
            ->belongsToMany('App\Funcion', 'rol_funcion', 'rol_id', 'funcion_id')
            ->withTimestamps();
    } 
}
