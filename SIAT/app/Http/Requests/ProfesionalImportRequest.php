<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfesionalImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ci_prof'  => 'string|nullable',
            'nom_prof' => 'required|string',
            'app_prof' => 'string|nullable',
            'apm_prof' => 'string',
            'dir_prof' => 'string|nullable',
            'cor_prof' => 'required|email|string|unique:profesional,cor_prof'
        ];
    }
}
