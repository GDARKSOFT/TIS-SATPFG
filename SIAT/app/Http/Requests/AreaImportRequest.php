<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AreaImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'unique:area,id|required|numeric',
            'nom_area' => 'unique:area,nom_area|required|string',
            'des_area' => 'nullable|string',
			'area_id' => 'exists:area,id|nullable|numeric'
        ];
    }

	/**
	 * Get the error messages for the defined validation rules.
	 *
	 * @return array
	 */
	public function messages()
	{
		return [
			'id.numeric' => 'El c�digo del �rea debe ser un valor num�rico',
            'id.required' => 'Debe introducir un valor para c�digo del �rea',
			'nom_area.required' => 'Debe introducir un valor para descripci�n del �rea',
			'nom_area.string' => 'El nombre del �rea debe contener solo letras',
			'des_area.string' => 'La descripci�n del �rea debe contener solo letras',
			'area_id.numeric' => 'El c�digo del �rea padre debe ser un valor n�merico'
		];
}
}
