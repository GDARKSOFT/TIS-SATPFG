<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Requests;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ci_user'  => 'max:100',
            'nom_user' => 'required|max:100',
            'app_user' => 'max:100',
            'apm_user' => 'max:100',
            'email' => 'required',
            'tel_user' => 'max:100',
            'dir_user' => 'max:100',
        ];
    }
}
