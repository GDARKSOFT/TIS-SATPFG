<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Requests\Requests;

class ProfesionalesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ci_prof'  => 'required',
            'nom_prof' => 'required',
            'app_prof' => 'required',
            'apm_prof' => 'required',
            'tel_prof' => 'required',
            'dir_prof' => 'required',
            'cor_prof' => 'required',
            'trab_prof'=> 'required'
        ];
    }
}
