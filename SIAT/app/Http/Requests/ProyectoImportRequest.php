<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoImportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
			'tit_proy'   => 'required|string',
			'nom_est' => 'required|string',
			'app_est' => 'string|nullable',
			'apm_est' => 'required|string',
			'carr_id' => 'required',
			'tut_id' => 'required',
			'mod_id' => 'required',
			'est_id' => 'required'
        ];
    }
}
