<?php

namespace App\Http\Controllers;
use App\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserFormRequest;
use Image;
use Auth;
use App\Profesional;
use App\Profesional_Competencia;
use App\Profesional_Interes;
use App\Area;
use App\Rol;
use App\BD;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Collection as Collection;
use Illuminate\Support\Facades\Validator;
use Hash;

class UserController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */   
    public function index()
    {
        return view('usuario.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function crearUser()
    {
        $roles = Rol::select()->get();
        return view('usuario.crear')->with('roles', $roles);       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $desrol = $request->input('user_rol');

        
        $prf = \DB::table('user')->select()->where('email','=',$request->email)->get();
        
        $rules = ['email'     => 'required',];
        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return ['created' => false,'errors'  => $validator->errors()->all()];
            }
            if (empty($prf->first())){
                if($desrol == 'Profesional'){
                    $profesional = new Profesional;
                    $profesional->ci_prof = $request->input('ci_user');
                    $profesional->nom_prof = $request->input('nom_user');
                    $profesional->app_prof = $request->input('app_user');
                    $profesional->apm_prof = $request->input('apm_user');
                    $profesional->cor_prof = $request->input('email');
                    $profesional->save();
                    $idp = $profesional->id_prof;
                }
               
                $rol = Rol::where('des_rol', $desrol)->first();

                $user = new User;
                $user->ci_user = $request->input('ci_user');
                $user->nom_user = $request->input('nom_user');
                $user->app_user = $request->input('app_user');
                $user->apm_user = $request->input('apm_user');
                $user->email = $request->input('email');
                $user->password = bcrypt('Usuario-123');
                if($desrol == 'Profesional'){
                    $user->prof_id = $idp;
                }else{
                    $user->prof_id = 0;
                }
                $user->save(); 
                $user->sendCuentaCreada();
                $user->roles()->attach($rol->id);
                Flash('Se ha creado un nuevo usuario correctamente.')->success();
                return view('usuario.index');
            }
            Flash::error("El correo electrónico ya existe.");
            return view('usuario.crear');
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $user = User::find($id);
        $irol = \DB::table('user_rol')->select('rol_id')->where('user_id','=', $user->id)->first();
        $rol = \DB::table('rol')->select('des_rol')->where('id','=', $irol->rol_id)->first();
        return view('usuario.mostrar', compact('user','rol'));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editarUser($id)
    {
        $user = User::find($id);
        $irol = \DB::table('user_rol')->select('rol_id')->where('user_id','=', $user->id)->first();
        $rol = \DB::table('rol')->select('id')->where('id','=', $irol->rol_id)->first();
        $rol = $rol->id;
        $iduser = $user->id;
        $roles = Rol::select()->get();
        
        return view('usuario.editar', compact('iduser','user','rol', 'roles')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $desrol = $request->input('user_rol');
        $rol = Rol::where('des_rol', $desrol)->first();
        $idrol = $rol->id;

        $user = User::find($id);
        $user->ci_user = $request->input('ci_user');
        $user->nom_user = $request->input('nom_user');
        $user->app_user = $request->input('app_user'); 
        $user->apm_user = $request->input('apm_user'); 
        $user->email = $request->input('email'); 

        \DB::update ('update user_rol Set rol_id = '.$idrol.' Where user_id='.$id.' ');
        if($user->prof_id != 0 && $desrol == 'Profesional'){//CUANDO 
            $profesional = Profesional::find($user->prof_id);
            $profesional->ci_prof = $request->input('ci_user');
            $profesional->nom_prof = $request->input('nom_user');
            $profesional->app_prof = $request->input('app_user');
            $profesional->apm_prof = $request->input('apm_user');
            $profesional->cor_prof = $request->input('email');
            $profesional->save();
        }
        if($user->prof_id == 0 && $desrol == 'Profesional'){
            $profesional = new Profesional; 
            $profesional->ci_prof = $request->input('ci_user');
            $profesional->nom_prof = $request->input('nom_user');
            $profesional->app_prof = $request->input('app_user');
            $profesional->apm_prof = $request->input('apm_user');
            $profesional->cor_prof = $request->input('email');
            $profesional->save();
            $user->prof_id = $profesional->id_prof;
        }
        if($user->prof_id != 0 && $desrol != 'Profesional'){
            Profesional::destroy($user->prof_id);
            $user->prof_id = 0;
        }
        $user->save();
        Flash('Se ha modificado los datos del usuario correctamente.')->success();
        return redirect()->route('usuario.index');
    } 

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function eliminarUser($id)
    {
        \DB::table('user')->select()->where('id',$id)->delete();
        return back();
    }

    public function getProfileAdministrador()
    {
        $user = Auth::user();
        return view('administrador.perfil')-> with('user', $user);
    }
    public function updateProfileAdministrador(UserFormRequest $request){
    	$user = Auth::user();
    	$user->ci_user = $request->input('ci_user');
    	$user->nom_user = $request->input('nom_user');
        $user->app_user = $request->input('app_user');
        $user->apm_user = $request->input('apm_user');
        $user->tel_user = $request->input('tel_user');
        $user->dir_user = $request->input('dir_user');
        $user->email = $request->input('email');
        $user->save();
        Flash('Se ha modificado su información personal correctamente.')->success();
        return view('home-usuario', array('user' => Auth:: user()));
    }
    public function updateFotoAdministrador(Request $request){
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/'.$filename));
            
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        
        return view('administrador.perfil', array('user' => Auth::user() ));
    }

    public function getProfileSecretaria()
    {
        $user = Auth::user();
        return view('secretaria.perfil')-> with('user', $user);
    }
    public function updateProfileSecretaria(UserFormRequest $request){
        $user = Auth::user();
        $user->ci_user = $request->input('ci_user');
        $user->nom_user = $request->input('nom_user');
        $user->app_user = $request->input('app_user');
        $user->apm_user = $request->input('apm_user');
        $user->tel_user = $request->input('tel_user');
        $user->dir_user = $request->input('dir_user');
        $user->email = $request->input('email');
        $user->save();
        Flash('Se ha modificado su información personal correctamente.')->success();
        return view('home-usuario', array('user' => Auth:: user()));
    }
    public function updateFotoSecretaria(Request $request){
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/'.$filename));
            
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        
        return view('secretaria.perfil', array('user' => Auth::user() ));
    }

    public function updateFotoConsejero(Request $request){
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/'.$filename));
            
            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }
        
        return view('consejero.perfil', array('user' => Auth::user() ));
    }
    
    public function getProfileConsejero()
    {
        $user = Auth::user();
        return view('consejero.perfil')-> with('user', $user);
    }
    public function updateProfileConsejero(UserFormRequest $request){
    	$user = Auth::user();
    	$user->ci_user = $request->input('ci_user');
    	$user->nom_user = $request->input('nom_user');
        $user->app_user = $request->input('app_user');
        $user->apm_user = $request->input('apm_user');
        $user->tel_user = $request->input('tel_user');
        $user->dir_user = $request->input('dir_user');
        $user->email = $request->input('email');
        $user->save();
        Flash('Se ha modificado su información personal correctamente.')->success();
        return view('home-usuario')-> with('user', $user);
    }

    public function competenciasMostrar()
    {
		$areas = Area::whereNull('area_id')->pluck('nom_area', 'id')->toArray();
		return view('profesional.competencias',compact('areas'));
    }

    public function competenciasGuardar(Request $request){
        $user = Auth::user();
        $profesional = Profesional::findOrFail($user->prof_id);
        $idarea = $request->input('subareas');
        if (is_null($idarea)){
            $idarea = $request->input('areas');
		}
		$profesional->competencias()->attach($idarea);
		Flash('Interes agregado correctamente.')->success();
        return redirect('/competencias'); 
    }

    public function getArea(Request $request, $id){
        if($request->ajax()){
            $areasFiltradas = Area::filtroSubarea($id);
            return response()->json($areasFiltradas);
        }

    }
//--------------------------------------------------------------------------------------------------
 public function interesMostrar()
    {
        $areas = Area::whereNull('area_id')->pluck('nom_area', 'id')->toArray();
		return view('profesional.interes',compact('areas'));
    }

    public function interesGuardar(Request $request){
        $user = Auth::user();
        $profesional = Profesional::findOrFail($user->prof_id);
        $idarea = $request->input('subareas');
        if (is_null($idarea)){
            $idarea = $request->input('areas');
		}
		$profesional->intereses()->attach($idarea);
		Flash('Interes agregado correctamente.')->success();
        return redirect('/interes'); 
    }
    public function password(){
        return view('profesional.password');
    }
    public function updatePassword(Request $request){
        $rules = [
            'mypassword' => 'required',
            'password' => 'required|confirmed|min:6|max:18',
        ];
        
        $messages = [
            'mypassword.required' => 'El campo es requerido',
            'password.required' => 'El campo es requerido',
            'password.confirmed' => 'Los passwords no coinciden',
            'password.min' => 'El mínimo permitido son 6 caracteres',
            'password.max' => 'El máximo permitido son 18 caracteres',
        ];
        
        $validator = Validator::make($request->all(), $rules, $messages);
        if ($validator->fails()){
            
            return redirect('User/password')->withErrors($validator);
        }
        else{
            if (Hash::check($request->mypassword, Auth::user()->password)){
                $user = new User;
                $user->where('email', '=', Auth::user()->email)
                     ->update(['password' => bcrypt($request->password)]);
                     Flash('Cambió de contraseña exitosamente.')->success();
                return redirect( route('home'))->with('status', 'Password cambiado con éxito');

            }
            else
            {
                Flash('La contraseña actual no coincide.')->error();
                return redirect('User/password')->with('message', 'Credenciales incorrectas');
            }
        }
    }

}