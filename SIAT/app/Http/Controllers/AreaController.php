<?php

namespace App\Http\Controllers;

use App\Area;
use App\Subarea;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\AreasFormRequest;
use Laracasts\Flash\Flash;

class AreaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Administrador');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('area.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('area.crear');       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ar = \DB::table('area')
                    ->select()
                    ->where('nom_area','=',$request->nom_area)
                    ->get();
        
        $rules = [
            'id'           => 'requerid',
            'nom_area'     => 'required',
 
             ];
        
        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            if (empty($ar->first())){
                $lastArea = Area::all()->last();
                if(is_null($lastArea))
                {
                    $id = 1;
                } else
                {
                    $id = $lastArea->id + 1;
                }
                $area = new Area;
                $area ->id = $id;
                $area ->nom_area = $request ->nom_area;
                $area ->des_area = $request ->des_area;
                $area ->save();
                Flash('Se ha creado una nueva área de conocimiento correctamente.')->success();
                return redirect()->route('areas.index');
            }
            Flash::error("El área de conocimiento " . $request->nom_area . " ya existe.");
            return view('area.crear');
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $area = Area::findOrFail($id);
        $subareas = $area->subareas()->get();
        return view('area.mostrar', compact('area', 'subareas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $area = Area::findOrFail($id);
        $subareas = $area->subareas()->get();
        return view('area.editar', compact('area', 'subareas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $area = Area::find($id);
        $area->nom_area = $request->input('nom_area');
        $area->des_area = $request->input('des_area'); 
        $area->save();
        Flash('Se ha modificado el área correctamente.')->success();
        return redirect()->route('areas.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Area::destroy($id);
        Flash('Se ha eliminado el área correctamente.')->success();
        return redirect()->route('areas.index');
    }

    public function createSubarea($id)
    {
        $area = Area::find($id);
        return view('subarea.crear', compact('area'));
    }

    public function storeSubarea(Request $request, $id)
    {

        $subAr = \DB::table('area')
                    ->select()
                    ->where('nom_area','=',$request->nom_subar)
                    ->get();
        
        $rules = [
            'nom_subar'     => 'required',
 
             ];
        
        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            if (empty($subAr->first())){
                $idSub = Area::all()->last()->id;
                $subarea = new Area;
                $subarea ->id = $idSub + 1;
                $subarea ->nom_area = $request->nom_subar;
                $subarea ->des_area = $request->des_subar;
                $area = Area::find($id);
                $area ->subareas() ->save($subarea);
                $subarea ->save();
                Flash('Se ha creado una nueva subárea correctamente.')->success();
                return redirect()->route('areas.edit', $id);
            }
            Flash::error("La subárea registrada ya existe.");
            return redirect()->route('areas.edit', $id);
           
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }

    }

    public function editSubarea($idArea, $idSubarea)
    {
        $area = Area::find($idArea);
        $subarea = Area::find($idSubarea);
        return view('subarea.editar', compact('area', 'subarea'));
    }

    public function updateSubarea(Request $request, $idArea, $idSubarea)
    {
        $subarea = Area::find($idSubarea);
        $subarea ->nom_area = $request ->nom_subar;
        $subarea ->des_area = $request ->des_subar;
        $subarea ->save();
        Flash('Se ha modificado una subárea correctamente.')->success();
        return redirect()->route('areas.edit', $idArea);
    }

    public function destroySubarea($idArea, $idSubarea)
    {
        Area::destroy($idSubarea);
        Flash('Se ha eliminado una subárea correctamente')->success();
        return redirect()->route('areas.edit', $idArea);
    }
}
