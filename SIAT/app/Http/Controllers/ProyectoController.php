<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Proyecto;
use App\Profesional;
use App\Proyecto_Area;
use App\Area;
use App\Modalidad;
use Illuminate\Support\Facades\Input;

class ProyectoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Administrador|Consejero');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['Administrador', 'Consejero']);
        return view('proyecto.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       $proyecto = Proyecto::find($id);
       $mod = Modalidad::All();
       $tutor = $proyecto->tut_id;
       $docente = $proyecto->doc_id;
       $idProy = $proyecto->id;
       $idModa = $proyecto->mod_id;

       $datosTutor = Profesional::where('id_prof','=',$tutor)->first();
       $datosDocente = Profesional::where('id_prof','=',$docente)->first();
       $datosProyAreas = Proyecto_Area::where('proy_id','=',$idProy)->first();
             $idArea = $datosProyAreas->area_id;
             $datosArea = Area::where('id','=',$idArea)->first();
       $datosProyModa = Modalidad::where('id','=',$idModa)->first();

       $proyReq = \DB::table('proyecto_requisito')->select('entregado')->where('proy_id','=',$id)->get();
        return view('proyecto.mostrar',compact('mod','proyecto','datosTutor','datosDocente','datosArea','datosProyModa','proyReq'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }
    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $cartaA = Input::has('opA')? true : false;
        $cartaB = Input::has('opB')? true : false;
        $cartaC = Input::has('opC')? true : false;
        $cartaD = Input::has('opD')? true : false;
        $cartaE = Input::has('opE')? true : false;
        
        if($cartaA){
            \DB::update ('update proyecto_requisito Set entregado=0 Where proy_id='.$id.' and req_id =1');
        }
        if($cartaB){
            \DB::update ('update proyecto_requisito Set entregado=0 Where proy_id='.$id.' and req_id =2'); 
        }
        if($cartaC){
            \DB::update ('update proyecto_requisito Set entregado=0 Where proy_id='.$id.' and req_id =3');
        }
        if($cartaD){
            \DB::update ('update proyecto_requisito Set entregado=0 Where proy_id='.$id.' and req_id =4'); 
        }
        if($cartaE){
            \DB::update ('update proyecto_requisito Set entregado=0 Where proy_id='.$id.' and req_id =5'); 
        }

        $proyecto = Proyecto::find($id);
        $idMod = Modalidad::find($proyecto->mod_id);
        if($idMod->des_mod == 'Proyecto de Grado' || $idMod->des_mod == 'Tesis'){
            if($cartaA && $cartaB && $cartaC && $cartaD){
                $proyecto->est_id = 2;
                $proyecto->save();
                Flash('Se habilitó el proyecto para la asignación de tribunales.')->success();
            }
        }else{
            if($cartaA && $cartaB && $cartaC && $cartaD && $cartaE){
                $proyecto->est_id = 2;
                $proyecto->save();
                Flash('Se habilitó el proyecto para la asignación de tribunales.')->success();
            }
        }
        return view('proyecto.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Proyecto::destroy($id);
        Flash('Se ha eliminado el proyecto correctamente.')->success();
        return redirect()->route('proyecto.index');
    }
}
