<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesional;
use App\Proyecto;
use App\User;
use App\Proyecto_Area;
use App\Area;
use App\Regla;
use App\Profesional_Competencia;
use Auth;
use Laracasts\Flash\Flash;

class AsignacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Consejero');
    }

    public function index()
    {
        return view('consejero.asignacion.index');
    }

    public function asignarTribunales($id)
    {
        return view('consejero.asignacion.asignar')-> with('id', $id);
    }

    public function reasignarTribunal($id, $idProfesional)
    {
        return view('consejero.asignacion.reasignar', compact('id', 'idProfesional'));
    }

    public function getTribunales($id)
    {
        return view('consejero.proyecto.tribunales')-> with('id', $id);
    }

    public function notificarTribunales(Request $request, $id)
    {
    		if (count($request->input('tribunales_seleccionados')) == 3)
    		{
    			$proyecto = Proyecto::findOrFail($id);
                $proyecto->est_id = 3;
                $proyecto->save(); 
    			$id_tribunales = $request->input('tribunales_seleccionados');
    			foreach($id_tribunales as $id_prof)
    			{
    				$tribunal = User::where('prof_id', $id_prof)->firstOrFail();
    				Profesional::findOrFail($id_prof)->proyectos()->attach($id);
    				$tribunal->sendNotificacionTribunales($proyecto);
                }
                $idUser = Auth::user()->id;
                $idRol = \DB::table('user_rol')->select('rol_id')->where('user_id','=',$idUser)->get();
                $rolUser = \DB::table('rol')->select('des_rol')->where('id','=',$idRol)->get();
                if($rolUser == 'Profesional'){
                    $tribunal->sendNotification($proyecto);
                }
    			Flash('Se ha asignado los tribunales correctamente.')->success();
    			return redirect()->route('asignacion.index');
    		} else
    		{
    			Flash::error("Porfavor, seleccione tres tribunales.");
                return redirect()->route('asignaciones.asignar', $id);
    		}
    }

    public function notificarTribunalReasignado(Request $request, $id, $idProfesional)
    {
        if (count($request->input('tribunales_seleccionados')) == 1)
        {
            $proyecto = Proyecto::findOrFail($id);
            $profesional = Profesional::findOrFail($idProfesional);
            $profesional->proyectos()->detach([$id]);
            $id_tribunales = $request->input('tribunales_seleccionados');
            foreach($id_tribunales as $id_prof)
            {
                $tribunal = User::where('prof_id', $id_prof)->firstOrFail();
                Profesional::findOrFail($id_prof)->proyectos()->attach($id);
                $tribunal->sendNotificacionTribunales($proyecto);
            }
            Flash('Se ha reasignado al tribunal correctamente.')->success();
            return redirect()->route('asignaciones.tribunales', $id);
        } else
        {
            Flash::error("Porfavor, seleccione un tribunal para reasignar.");
            return redirect()->route('asignaciones.reasignar', ['id' => $id, 'idProfesional' => $idProfesional]);
        }
    }

    public function getProyectosAsignados()
    {
        return view('consejero.proyecto.asignados');
    }
    public function proyectoDefendido($id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $areas = $proyecto->areas()->get();
        $profesionales = Profesional::all();
        for ($i=0,$size = count($profesionales); $i < $size ; $i++)
        {
            $profesional = $profesionales[$i];
            if(!$profesional->tieneCompetenciaEn($areas)&&!$profesional->tieneInteresEn($areas)||!$profesional->tieneProyecto($proyecto))
            {
                unset($profesionales[$i]);
            }
        }
        \DB::table('profesional_proyecto')->select()->where('proy_id',$id)->delete();
        $proyecto->est_id = 4;
        $proyecto->save();
        Flash('EL proyecto esta defendido')->success();
        return view('consejero.proyecto.asignados');
    }
 
}
