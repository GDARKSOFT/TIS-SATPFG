<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Regla;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ReglasFormRequest;
use App\Http\Requests\EditarReglasRequest;
use Laracasts\Flash\Flash;
use Auth;
class ReglaController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Administrador|Consejero');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $request->user()->authorizeRoles(['Administrador', 'Consejero']);
        return view('regla.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function create()
    {
        return view('regla.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */   
    public function store(ReglasFormRequest $request){

        
        $reg = \DB::table('regla')->select()->where('des_reg','=',$request->des_reg)->get();
        
           $rules = [
            'des_reg'     => 'required',
            'val_reg'  => 'required'
            ];
        
        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            if (empty($reg->first())){
                $regla = $request->all();
                $regla = Regla::create($regla);
                $regla->save();
                Flash('Se ha creado una nueva regla.')->success();
                return redirect('/reglas');   
            }
            Flash::error("Error no se puede guardar, la regla ya existe.");
            return view('regla.crear');
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }
        


    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function show($id)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $regla = Regla::find($id);
        return view('regla.editar')-> with('regla', $regla);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditarReglasRequest $request, $id)
    {
        
        $regla = Regla::find($id);
        $regla-> fill($request->all());
        $regla-> save();
        Flash('Se ha modificado la regla correctamente.')->success();
        return redirect()->route('reglas.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('regla')->select()->where('id_reg',$id)->delete();
        Flash('Se ha eliminado una regla correctamente.')->success();
        return back();
    }
}
