<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profesional;
use App\User;
use App\Area;
use App\Rol;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests\ProfesionalesFormRequest;
use Laracasts\Flash\Flash;
use Auth;
use Image;
class ProfesionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Administrador|Profesional');
    }

    public function index()
    {
        return view('profesional.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $are = Area::all();
        return view('profesional.crear',compact('are'));

    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $prf = \DB::table('profesional')
                    ->select()
                    ->where('cor_prof','=',$request->cor_prof)
                    ->get();

        $rules = [
            'cor_prof'     => 'required',
             ];


        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            if (empty($prf->first())){
                $profesional = new Profesional;
                $profesional->ci_prof = $request->input('ci_prof');
                $profesional->nom_prof = $request->input('nom_prof');
                $profesional->app_prof = $request->input('app_prof');
                $profesional->apm_prof = $request->input('apm_prof');
                $profesional->tel_prof = $request ->input('tel_prof');
                $profesional->dir_prof = $request ->input('dir_prof');
                $profesional->cor_prof = $request->input('cor_prof');
                $profesional ->trab_prof = $request ->input('trab_prof');
                $profesional->save();
                $idp = $profesional->id_prof;
                $rol = Rol::where('des_rol', 'Profesional')->first();
                $user = new User;
                $user->ci_user = $request->input('ci_prof');
                $user->nom_user = $request->input('nom_prof');
                $user->app_user = $request->input('app_prof');
                $user->apm_user = $request->input('apm_prof');
                $user->tel_user = $request->input('tel_prof');
                $user->dir_user = $request->input('dir_prof');
                $user->email = $request->input('cor_prof');
                $user->password = bcrypt('Usuario-123');
                $user->prof_id = $idp;
                $user->save();
                //----------------------------------------------------------
                $tribunal = Profesional::findOrFail($idp);
                $ciTribunal = $tribunal->id_prof;
                $tribunalUser = User::where('prof_id',$ciTribunal)->get()->first();
                $tribunalUser->sendCuentaCreada();
                //----------------------------------------------------------
                $user->roles()->attach($rol->id);
                Flash('Se ha creado un nuevo profesional correctamente.')->success();
                return redirect('/profesionales');
            }
            Flash::error("El correo electronico registrado ya existe.");

            return view('profesional.crear');
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }

 
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profesional = Profesional::find($id);
        return view('profesional.mostrar')->with('profesional',$profesional);
        //return view("profesionales",["profesional"=>Profesional::findOrFail($id)]);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $profesional = Profesional::find($id);
        return view('profesional.editar')->with('profesional',$profesional);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $profesional = Profesional::find($id);$request->input('ci_prof');
                $profesional->nom_prof = $request->input('nom_prof');
                $profesional->app_prof = $request->input('app_prof');
                $profesional->apm_prof = $request->input('apm_prof');
                $profesional->tel_prof = $request ->input('tel_prof');
                $profesional->dir_prof = $request ->input('dir_prof');
                $profesional->cor_prof = $request->input('cor_prof');
                $profesional->save();
        Flash('Se ha modificado al profesional correctamente.')->success();
        return redirect()->route('profesionales.index');
    }
     /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('profesional')->select()->where('id_prof',$id)->delete();
        return back();
    }
    /**
     * USER
     */
    public function showProfesional(){
        $user = Auth::user()->prof_id;
        $profesional = Profesional::find($user);
        return view ('profesional.perfil',array('user'=>Auth::user()))->with('profesional',$profesional);
    }
    public function updateFotoProfesional(Request $request){
        if ($request->hasFile('avatar')) {
            $avatar = $request->file('avatar');
            $filename = time().'.'.$avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( public_path('uploads/avatars/'.$filename));

            $user = Auth::user();
            $user->avatar = $filename;
            $user->save();
        }

        $user = Auth::user()->prof_id;
        $profesional = Profesional::find($user);
        $fotUser = Auth::user()->avatar;
        $profesional->avatar = $fotUser;
        $profesional->save();
        return view('profesional.perfil', array('user' => Auth::user() ))->with('profesional',$profesional);
    }
    public function updateProfesional(Request $request,$id)
    {
        $profesional = Profesional::find($id);
        $profesional -> fill($request->all());
        $profesional->save();
        
        $user = Auth::user();
                $user->ci_user = $request->input('ci_prof');
                $user->nom_user = $request->input('nom_prof');
                $user->app_user = $request->input('app_prof');
                $user->apm_user = $request->input('apm_prof');
                $user->tel_user = $request->input('tel_prof');
                $user->dir_user = $request->input('dir_prof');
        $user-> save();
        Flash('Se ha modificado al profesional correctamente.')->success();
        return view ('home-usuario');
    }

    public function getProyectos()
    {
        return view ('profesional.proyectos');
    }
}