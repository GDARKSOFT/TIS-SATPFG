<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laracasts\Flash\Flash;
use App\Rol;
use App\Funcion;
use Illuminate\Support\Facades\Input;

class RolController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('role:Administrador|Consejero');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('rol.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */    
    public function create()
    {
        return view('rol.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */   
    public function store(Request $request){

        
        $rol = \DB::table('rol')->select()->where('des_rol','=',$request->des_rol)->get();
        
           $rules = [
            'des_rol'     => 'required',
            ];
        
        $fun1 = Input::has('op1')? true : false;
        $fun2 = Input::has('op2')? true : false;
        $fun3 = Input::has('op3')? true : false;
        $fun4 = Input::has('op4')? true : false;
        $fun5 = Input::has('op5')? true : false;
        $fun6 = Input::has('op6')? true : false;
        $fun7 = Input::has('op7')? true : false;
        $fun8 = Input::has('op8')? true : false;
        $fun9 = Input::has('op9')? true : false;
        $fun10 = Input::has('op10')? true : false;
        $fun11= Input::has('op11')? true : false;
        $fun12 = Input::has('op12')? true : false;

        $fun_1 = Funcion::where('des_fun', 'Home')->first();
        $fun_2 = Funcion::where('des_fun', 'Usuarios')->first();
        $fun_3 = Funcion::where('des_fun', 'Roles')->first();
        $fun_4 = Funcion::where('des_fun', 'Reglas')->first();
        $fun_5 = Funcion::where('des_fun', 'Areas')->first();
        $fun_6 = Funcion::where('des_fun', 'Profesionales')->first();
        $fun_7 = Funcion::where('des_fun', 'Proyectos')->first();
        $fun_8 = Funcion::where('des_fun', 'Proyectos Asignados')->first();
        $fun_9 = Funcion::where('des_fun', 'Proyectos Habilitados')->first();
        $fun_10 = Funcion::where('des_fun', 'Proyectos a Evaluar')->first();
        $fun_11 = Funcion::where('des_fun', 'Competencias')->first();
        $fun_12 = Funcion::where('des_fun', 'Intereses')->first();
       
		        
        try {
            // Ejecutamos el validador y en caso de que falle devolvemos la respuesta
            $validator = \Validator::make($request->all(), $rules);
            if ($validator->fails()) {
                return [
                    'created' => false,
                    'errors'  => $validator->errors()->all()
                ];
            }
            if (empty($rol->first())){
            	if(!$fun1 && !$fun2 && !$fun3 && !$fun4 && !$fun5 && !$fun6 && 
            	   !$fun7 && !$fun8 && !$fun9 && !$fun10 && !$fun11 && !$fun12){
            		Flash::error("Error debe seleccionar alguna funcionalidad para el nuevo rol.");
           		    return view('rol.crear');
            	}else{
	            	$nrol = new Rol;
	                $nrol ->des_rol = $request->input('des_rol');
	                $nrol ->save();

			        if($fun1){
			            $nrol->funciones()->attach($fun_1->id);
			        }
			        if($fun2){
			            $nrol->funciones()->attach($fun_2->id);
			        }
			        if($fun3){
			            $nrol->funciones()->attach($fun_3->id);
			        }
			        if($fun4){
			            $nrol->funciones()->attach($fun_4->id);
			        }
			        if($fun5){
			            $nrol->funciones()->attach($fun_5->id);
			        }
			        if($fun6){
			            $nrol->funciones()->attach($fun_6->id);
			        }
			        if($fun7){
			            $nrol->funciones()->attach($fun_7->id);
			        }
			        if($fun8){
			            $nrol->funciones()->attach($fun_8->id);
			        }
			        if($fun9){
			            $nrol->funciones()->attach($fun_9->id);
			        }
			        if($fun10){
			            $nrol->funciones()->attach($fun_10->id);
			        }
			        if($fun11){
			            $nrol->funciones()->attach($fun_11->id);
			        }
			        if($fun12){
			            $nrol->funciones()->attach($fun_12->id);
			        }

	                Flash('Se ha creado una nuevo rol.')->success();
	           		return view('rol.index');   
            }
        }
            Flash::error("Error no se puede guardar, el rol ya existe.");
            return view('rol.crear');
        } catch (Exception $e) {
            \Log::info('Error creating role: '.$e);
            return \Response::json(['created' => false], 500);
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */    
    public function show($id)
    {
        $rol = Rol::findOrFail($id);
        $funciones = $rol->funciones()->get();
        
        return view('rol.mostrar', compact('rol', 'funciones'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rol = Rol::find($id);

        $funciones = Funcion::select()->get();
        $arreglo = array();
        $i = 0;
        foreach ($funciones as $fun) {
        	$funcion = \DB::select('select rol_id from rol_funcion where rol_id ='.$id.' and funcion_id='.$fun->id.'');

        	$arreglo[$i] = 0;
        	if ($funcion == null){
        	    $arreglo[$i] = 1;
        	}
        	$i = $i + 1;
        }
        //dd($arreglo);
        return view('rol.editar', compact('rol', 'arreglo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $fun1 = Input::has('op1')? true : false;
        $fun2 = Input::has('op2')? true : false;
        $fun3 = Input::has('op3')? true : false;
        $fun4 = Input::has('op4')? true : false;
        $fun5 = Input::has('op5')? true : false;
        $fun6 = Input::has('op6')? true : false;
        $fun7 = Input::has('op7')? true : false;
        $fun8 = Input::has('op8')? true : false;
        $fun9 = Input::has('op9')? true : false;
        $fun10 = Input::has('op10')? true : false;
        $fun11 = Input::has('op11')? true : false;
        $fun12 = Input::has('op12')? true : false;

        $fun_1 = Funcion::where('des_fun', 'Home')->first();
        $fun_2 = Funcion::where('des_fun', 'Profesionales')->first();
        $fun_3 = Funcion::where('des_fun', 'Usuarios')->first();
        $fun_4 = Funcion::where('des_fun', 'Roles')->first();
        $fun_5 = Funcion::where('des_fun', 'Reglas')->first();
        $fun_6 = Funcion::where('des_fun', 'Areas')->first();
        $fun_7 = Funcion::where('des_fun', 'Proyectos')->first();
        $fun_8 = Funcion::where('des_fun', 'Proyectos Asignados')->first();
        $fun_9 = Funcion::where('des_fun', 'Proyectos Habilitados')->first();
        $fun_10 = Funcion::where('des_fun', 'Competencias')->first();
        $fun_11 = Funcion::where('des_fun', 'Intereses')->first();
        $fun_12 = Funcion::where('des_fun', 'Proyectos a Evaluar')->first();
        
        $rol = Rol::find($id);
        $rol ->des_rol = $request->input('des_rol');
        $rol-> save();

        \DB::delete('delete from rol_funcion where rol_id='.$id.'');
        //dd($rol);
        if(!$fun1 && !$fun2 && !$fun3 && !$fun4 && !$fun5 && !$fun6 && 
           !$fun7 && !$fun8 && !$fun9 && !$fun10 && !$fun11 && !$fun12){
    		Flash::error("Error debe seleccionar alguna funcionalidad.");
   		    return view('rol.editar');
        }else{
    			if($fun1){
		            $rol->funciones()->attach($fun_1->id);
		        }
		        if($fun2){
		            $rol->funciones()->attach($fun_2->id);
		        }
		        if($fun3){
		            $rol->funciones()->attach($fun_3->id);
		        }
		        if($fun4){
		            $rol->funciones()->attach($fun_4->id);
		        }
		        if($fun5){
		            $rol->funciones()->attach($fun_5->id);
		        }
		        if($fun6){
		            $rol->funciones()->attach($fun_6->id);
		        }
		        if($fun7){
		            $rol->funciones()->attach($fun_7->id);
		        }
		        if($fun8){
		            $rol->funciones()->attach($fun_8->id);
		        }
		        if($fun9){
		            $rol->funciones()->attach($fun_9->id);
		        }
		        if($fun10){
		            $rol->funciones()->attach($fun_10->id);
		        }
		        if($fun11){
		            $rol->funciones()->attach($fun_11->id);
		        }
		        if($fun12){
		            $rol->funciones()->attach($fun_12->id);
		        }
		    }

	    Flash('Se ha modificado el rol correctamente.')->success();
	    return redirect()->route('rol.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        \DB::table('rol')->select()->where('id',$id)->delete();
        Flash('Se ha eliminado el rol correctamente.')->success();
        return view('rol.index');
    }
}
