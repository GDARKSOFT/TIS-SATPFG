<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CsvImportRequest;
use App\Http\Requests\AreaImportRequest;
use App\Http\Requests\ProfesionalImportRequest;
use App\Http\Requests\ProyectoImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Routing\Redirector;
use App\Area;
use App\User;
use App\Rol;
use App\Subarea;
use App\Profesional;
use App\Proyecto;
use App\Modalidad;
use App\Carrera;
use App\Estado;
use Laracasts\Flash\Flash;
use Exception;
use Validator;

class ExcelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function importarAreas(CsvImportRequest $request)
    {
		ini_set('max_execution_time', 300);
		$file = $request->excel;	  
		Excel::filter('chunk')->load($file, false, 'ISO-8859-1')->chunk(500, function($results)
		{
			$errores = array();
			$line = 0;
            foreach ($results->toArray() as $row)
            {
				$line ++;
				try{				
					$area = array('id'   => $row['codigo'],
								  'nom_area' => $row['nombre'],
								  'des_area' => $row['descripcion'],
								  'area_id' => $row['cod_are']);
                
					$csv_errors = Validator::make(
					  $area, 
					  (new AreaImportRequest)->rules()
					)->errors();
    
					if ($csv_errors->any()) {
						foreach($csv_errors as $error){
							$errores[] = $error;
						}
					} else{
						Area::create($area);
					}
				} catch(ModelNotFoundException $e){
					$errores[] = $e;
				} catch(Exception $e){
					$errores[] = $e;
				}
            }
		}, false);
		Flash('Datos importados correctamente.')->success();
        return redirect('/areas');        
    }

    public function importarProfesionales(CsvImportRequest $request)
    {
		ini_set('max_execution_time', 300);
		$file = $request->excel;	  
		Excel::filter('chunk')->load($file, false, 'ISO-8859-1')->chunk(500, function($results)
        {
			$errores = array();
			$line = 0;
            foreach ($results->toArray() as $row)
			{
				try{
					$profesional = array('ci_prof'   => $row['ci'],
								  'nom_prof' => $row['nombre'],
								  'app_prof' => $row['ape_pat'],
								  'apm_prof' => $row['ape_mat'],
								  'tel_prof' => $row['telefono'],
								  'dir_prof' => $row['direccion'],
								  'cor_prof' => $row['correo']);
					$csv_errors = Validator::make(
						$profesional, 
						(new ProfesionalImportRequest)->rules()
					)->errors();
					if ($csv_errors->any()) {
						foreach($csv_errors as $error){
							$errores[] = $error;
						}
					} else {
						$profesionalSave = Profesional::create($profesional);
						$rolTabla = Rol::where('des_rol','Profesional')->first();
						$cuentaUser = User::firstOrCreate(
							['nom_user' => $row['nombre'],
							'email' => $row['correo'],
							'password' =>bcrypt('Usuario-123'),
							'prof_id' => $profesionalSave->id_prof]);
						$cuentaUser->save();
						$idRolUser = $cuentaUser->roles()->attach($rolTabla->id);
					}					
				} catch(Exception $e){
					$errores[] = $e;
				}
            }
        }, false);
        Flash('Datos importados correctamente.')->success();
        return redirect('/profesionales');
    }
    public function importarProyectos(CsvImportRequest $request)
    {
		ini_set('max_execution_time', 300);
		$file = $request->excel;
		Excel::filter('chunk')->load($file, false, 'ISO-8859-1')->chunk(500, function($results)
        {
            $errores = array();
			$line = 0;
            foreach ($results->toArray() as $row)
			{
				try{
					$tutor = Profesional::where('app_prof' ,$row['apellido_paterno_tutor'])->where('apm_prof' ,$row['apellido_materno_tutor'])->firstOrFail();
					$area = Area::where('nom_area', $row['area'])->firstOrFail();
					$modalidad = Modalidad::where('des_mod', $row['modalidad_titulacion'])->firstOrFail();
					$carrera = Carrera::where('des_carr', $row['carrera'])->firstOrFail();
					$proyecto = array('tit_proy'   => $row['titulo_proyecto_final'],
								  'nom_est' => $row['nombre_postulante'],
								  'app_est' => $row['apellido_paterno_postulante'],
								  'apm_est' => $row['apellido_materno_postulante'],
								  'carr_id' => $carrera->id,
								  'tut_id' => $tutor->id_prof,
								  'mod_id' => $modalidad->id,
								  'est_id' => 1);
					$csv_errors = Validator::make(
						$proyecto, 
						(new ProyectoImportRequest)->rules()
					)->errors();
					if ($csv_errors->any()) {
						foreach($csv_errors as $error){
							$errores[] = $error;
						}
					} else {
						$proyectoSave = Proyecto::firstOrCreate($proyecto);
						$proyectoSave->areas()->attach($area->id);
						\DB::insert('insert into proyecto_requisito(proy_id,req_id) values (?,?)',[$proyectoSave->id,1]);
						\DB::insert('insert into proyecto_requisito(proy_id,req_id) values (?,?)',[$proyectoSave->id,2]);
						\DB::insert('insert into proyecto_requisito(proy_id,req_id) values (?,?)',[$proyectoSave->id,3]);
						\DB::insert('insert into proyecto_requisito(proy_id,req_id) values (?,?)',[$proyectoSave->id,4]);
						if($proyectoSave->mod_id == 3 || $proyectoSave->mod_id == 4){
							\DB::insert('insert into proyecto_requisito(proy_id,req_id) values (?,?)',[$proyectoSave->id,5]); 
						}
					}					
				} catch(Exception $e){
					$errores[] = $e;
				}
            }
        }, false);
        Flash('Datos importados correctamente.')->success();
        return redirect('/proyectos');
    }
}