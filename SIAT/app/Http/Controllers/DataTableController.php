<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Regla;
use App\Rol;
use App\User;
use App\Area;
use App\Profesional;
use App\Proyecto;
use Auth;
use Yajra\Datatables\Datatables;

class DataTableController extends Controller
{
    public function getReglas()
    {
        $reglas = Regla::select(['id_reg','des_reg', 'val_reg']);
        return Datatables::of($reglas)
            ->addColumn('action', function ($regla) {
                return
                '<a href="'. route('reglas.edit', $regla->id_reg) .'" class="colorIcon marginIcon"><i class="fas fa-pencil-alt fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getAreas()
    {
        $areas = Area::where('area_id', null)->select(['id','nom_area']);
        return Datatables::of($areas)
            ->addColumn('action', function ($area) {
                return
                '<a href="'. route('areas.show', $area->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>'.
                '<a href="'. route('areas.edit', $area->id) .'" class="colorIcon marginIcon"><i class="fas fa-pencil-alt fa-lg"></i></a>'.
                '<a href="#" class="colorIcon delete-modal marginIcon"  data-toggle="modal" data-target="#modal-delete" data-delete-link="'. route('areas.destroy', $area->id) .'"
                data-body="Esta seguro de eliminar el area: '. $area ->id .'"><i class="fas fa-trash-alt fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getProfesionales()
    {
        $profesionales = Profesional::select(['id_prof', 'ci_prof','nom_prof','app_prof', 'apm_prof', 'cor_prof' ]);
        return Datatables::of($profesionales)
			->add_column('proyectos', function(Profesional $profesional) {
				return $profesional->proyectos()->count();
			})
            ->addColumn('action', function ($profesional) {
                return
                '<a href="'. route('profesionales.show', $profesional->id_prof) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>'.
                '<a href="'. route('profesionales.edit', $profesional->id_prof) .'" class="colorIcon marginIcon"><i class="fas fa-pencil-alt fa-lg"></i></a>'.
                '<a href="#" class="colorIcon delete-modal marginIcon" data-toggle="modal" data-target="#modal-delete" data-delete-link="'. route('profesionales.destroy', $profesional->id_prof) .'"
                data-body="Esta seguro de eliminar al profesional: '. $profesional->id_prof .'"><i class="fas fa-trash-alt fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getProyectos()
    {
        $proyectos = Proyecto::with('carrera')->with('modalidad')->with('estado')->select(['id', 'carr_id', 'tit_proy', 'nom_est','app_est','apm_est', 'mod_id', 'est_id']);
        return Datatables::of($proyectos)
            ->addColumn('action', function (Proyecto $proyecto) {
                return
                '<a href="'. route('proyectos.show', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getProyectosAsignacion()
    {
        $proyectos = Proyecto::where('est_id', 2)->with('carrera')->with('modalidad')->with('areas')->select(['id', 'carr_id', 'tit_proy', 'nom_est','app_est','apm_est', 'mod_id']);
        return Datatables::of($proyectos)
            ->addColumn('action', function (Proyecto $proyecto) {
                return
				'<a href="'. route('proyectos.show', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>'.
                '<a href="'. route('asignaciones.asignar', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fas fa-address-book fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getProyectosAsignados()
    {
        $proyectos = Proyecto::where('est_id', 3)->with('carrera')->with('modalidad')->select(['id', 'carr_id', 'tit_proy', 'nom_est','app_est','apm_est', 'mod_id']);
        return Datatables::of($proyectos)
            ->addColumn('action', function (Proyecto $proyecto) {
                return
                '<a href="'. route('asignaciones.tribunales', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fas fa-users fa-lg"></i></a>'.
                '<a href="'. route('asignaciones.defendido', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fas fa-hourglass-end"></i></a>';
            })
            ->make(true);
    }

    public function getTribunalesAsignados($id)
    {
        $proyecto = Proyecto::findOrFail($id);
        $profesionales = $proyecto->profesionales()->get();
        return Datatables::of($profesionales)
			->add_column('proyectos', function(Profesional $profesional){
				return $profesional->proyectos()->count();
			})
            ->addColumn('action', function (Profesional $profesional) use($id){
                return
                '<a href="'. route('asignaciones.reasignar', ['id' => $id, 'idProfesional' => $profesional->id_prof]) .'" class="colorIcon marginIcon">Reasignar</a>';
            })
            ->make(true);
    }

    public function getTribunales($id)
    {
        $numPermitido = Regla::find(1)->val_reg;
		$proyecto = Proyecto::findOrFail($id);
        $areas = $proyecto->areas()->get()->toArray();
        $profesionales = Profesional::all();
        for ($i=0, $size = count($profesionales); $i < $size ; $i++)
        {
            $profesional = $profesionales[$i];
			$numproy = $profesional->proyectos()->count();
            if(!$profesional->tieneCompetenciaEn($areas)&&!$profesional->tieneInteresEn($areas)||$profesional->tieneProyecto($proyecto)||$numproy>$numPermitido)
            {
                unset($profesionales[$i]);
            }
        }
        return Datatables::of($profesionales)
			->add_column('proyectos', function(Profesional $profesional) {
				return $profesional->proyectos()->count();
			})
            ->addColumn('check', '<input type="checkbox" name="tribunales_seleccionados[]" value="{{ $id_prof }}">')
            ->make(true);
    }

    public function getUsuarios()
    {
        $usuarios = User::select(['id', 'ci_user','nom_user','app_user', 'apm_user', 'email' ]);

        return Datatables::of($usuarios)
            ->add_column('roles', function(User $user) {
                return $user->roles()->first()->des_rol;
            })
            ->addColumn('action', function ($usuario) {
                return
                '<a href="'. route('usuario.show', $usuario->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>'.
                '<a href="'. route('usuario.edit', $usuario->id) .'" class="colorIcon marginIcon"><i class="fas fa-pencil-alt fa-lg"></i></a>'.
                '<a href="#" class="colorIcon delete-modal marginIcon" data-toggle="modal" data-target="#modal-delete" data-delete-link="'. route('usuario.destroy', $usuario->id) .'"
                data-body="Esta seguro de eliminar al usuario: '. $usuario->id .'"><i class="fas fa-trash-alt fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getRoles()
    {
        $roles = Rol::select(['id', 'des_rol']);
        return Datatables::of($roles)
            ->addColumn('action', function (Rol $rol) {
                return
                '<a href="'. route('rol.show', $rol->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>'.
                '<a href="'. route('rol.edit', $rol->id) .'" class="colorIcon marginIcon"><i class="fas fa-pencil-alt fa-lg"></i></a>'.
                '<a href="#" class="colorIcon delete-modal marginIcon"  data-toggle="modal" data-target="#modal-delete" data-delete-link="'. route('rol.destroy', $rol->id) .'"
                data-body="Esta seguro de eliminar el rol: '. $rol ->id .'"><i class="fas fa-trash-alt fa-lg"></i></a>';
            })
            ->make(true);
    }

    public function getProyectosProfesional()
    {
        $user = Auth::user();
        $profesional = Profesional::findOrFail($user->prof_id);
        $proyectos = $profesional->proyectos()->get();
        return Datatables::of($proyectos)
            ->addColumn('action', function (Proyecto $proyecto) {
                return
                '<a href="'. route('proyectos.show', $proyecto->id) .'" class="colorIcon marginIcon"><i class="fa fa-eye fa-lg"></i></a>';
            })
            ->make(true);
    }

	public function getCompetencias(){
		$user = Auth::user();
        $profesional = Profesional::findOrFail($user->prof_id);
		$competencias = $profesional->competencias()->get();
		return Datatables::of($competencias)
			->make(true);
	}

	public function getIntereses(){
		$user = Auth::user();
        $profesional = Profesional::findOrFail($user->prof_id);
		$intereses = $profesional->intereses()->get();
		return Datatables::of($intereses)
			->make(true);
	}
    
}
