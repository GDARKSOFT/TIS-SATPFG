<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Estado extends Model
{
    public $table = "estado";
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'des_est'];

	public function proyectos()
    {
        return $this
            ->hasMany('App\Proyecto', 'est_id');
    }
}
