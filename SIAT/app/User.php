<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\NombramientoTribunal;
use App\Notifications\CuentaCreada;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable 
{
    use Notifiable;

    public $table = "user";
    public function roles()
    {
        return $this
            ->belongsToMany('App\Rol', 'user_rol', 'user_id', 'rol_id')
            ->withTimestamps();
    }

    public function authorizeRoles($roles)
    {
        if ($this->hasAnyRole($roles)) {
            return true;
        }
        abort(401, 'Esta acción no está autorizada.');
    }

    public function hasAnyRole($roles)
    {
        if (is_array($roles)) {
            foreach ($roles as $role) {
                if ($this->hasRole($role)) {
                    return true;
                }
            }
        } else {
            if ($this->hasRole($roles)) {
                return true;
            }
        }
        return false;
    }
    public function hasRole($role)
    {
        if ($this->roles()->where('des_rol', $role)) {
            return true;
        }
        return false;
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nom_user', 'email', 'password','prof_id',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */ 
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendNotificacionTribunales(Proyecto $proyecto)
    {
        $this->notify(new NombramientoTribunal($proyecto));
    }

    public function sendNotificacion(Proyecto $proyecto)
    {
        $this->notify(new NombramientoTribunal($proyecto));
    }
     public function sendCuentaCreada()
    {
        $this->notify(new CuentaCreada());
    }
}
