<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyecto_Area extends Model
{
    public $table = "proyecto_area";
    protected $primaryKey = 'id';
    protected $fillable = ['proy_id','area_id'];
}
