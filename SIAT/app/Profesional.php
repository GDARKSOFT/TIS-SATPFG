<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profesional extends Model
{
    protected $table = 'profesional'; 

    protected $primaryKey = 'id_prof';

    protected $fillable = [ 'ci_prof',
                            'cod_prof',
                            'nom_prof',
                            'app_prof',
                            'apm_prof',
                            'tel_prof',
                            'dir_prof',
                            'cor_prof',
                            'trab_prof',];
    public function competencias()
    {
        return $this
            ->belongsToMany('App\Area','profesional_competencia','prof_id','subar_id')
            ->withTimestamps();
    }

    public function intereses()
    {
        return $this
            ->belongsToMany('App\Area','profesional_interes','prof_id','subar_id')
            ->withTimestamps();
    }

	public function proyectos()
	{
		return $this
			->belongsToMany('App\Proyecto', 'profesional_proyecto', 'prof_id', 'proy_id')
            ->withTimestamps();
	}

   public function tieneCompetenciaEn($areas)
    {
        if (is_array($areas)) {
            foreach ($areas as $area) {
                if ($this->tieneCompetencia($area['id'])) {
                    return true;
                } else {
					if(!is_null($area['area_id']))
					{
						if($this->tieneCompetencia($area['area_id']))
						{
							return true;
						}
					}
				}
            }
        }
        return false;
    }
    public function tieneCompetencia($id)
    {
		if ($this->competencias()->where('id', $id)->first() || $this->competencias()->where('area_id', $id)->first())
		{
			return true;
        }
        return false;
    }


    public function tieneInteresEn($areas)
    {
        if (is_array($areas)) {
            foreach ($areas as $area) {			
                if ($this->tieneInteres($area['id'])) {
                    return true;
                } else {
					if(!is_null( $area['area_id']))
					{
						if($this->tieneInteres($area['area_id']))
						{
							return true;
						}
					}
				}
            }
        }
        return false;
    }
    public function tieneInteres($id)
    {
        if ($this->intereses()->where('id', $id)->first() || $this->intereses()->where('area_id', $id)->first())
		{
			return true;
        }
        return false;
    }

    public function tieneProyecto($proyecto)
    {
        if ($this->proyectos()->where('id', $proyecto->id)->first())
        {
            return true;
        }
        return false;
    }
}