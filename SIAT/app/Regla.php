<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Regla extends Model

{ 
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   

   

    protected $table = 'regla'; 

    protected $primaryKey = 'id_reg';

    protected $fillable =[
    	'des_reg',
    	'val_reg'
    ];

    protected $guarded =[

    ];

}
