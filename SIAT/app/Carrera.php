<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrera extends Model
{
    public $table = "carrera";
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'des_carr'];

    public function proyectos()
    {
        return $this
            ->hasMany('App\Proyecto', 'carr_id');
    }
}
