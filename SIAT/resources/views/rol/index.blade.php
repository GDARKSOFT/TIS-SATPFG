@extends('master')
@section('title', 'Roles')
@section('content')
@extends('cabecera-con-sesion-usuario')

<!-- Image and text -->
<h2  class="title">ROLES</h2>
	<div class="container">
		@include('flash::message')
		<div class = "containerTable">
			<table id="tablaRol" class="table table-hover table-bordered">
				<thead class="headTableColor">
				<tr>
					<th scope="col">Id</th>
                    <th scope="col">Rol</th>
                    <th>Acciones</th>
					
				</tr>
				</thead>
			</table>
		</div>
		<br>
		
		<div class="btnFormat">
			<div class="row" style="text-align: center">
				<div class="col-md-2"><button type="button" onclick="history.back(-1)" class="btn btn-primary changeColorBtn btnColor">Atrás</button></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-2"><a href="{{ route('rol.create') }}" class="btn btn-primary changeColorBtn btnColor">nuevo rol</a></div>
			</div>
		</div>	

	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		  $(function() {
		  $('#tablaRol').DataTable({
		      processing: true,
		      serverSide: true,
		      ajax: "{{route('datatable.roles')}}",
		      columns: [
		          { data: 'id', name: 'id' },
		          { data: 'des_rol', name: 'des_rol' },
		          { data: 'action', name: 'action', orderable: false, searchable: false }
		      ],
		      language: esp
		  });
		});
	</script>
@endsection