@extends('master')
@section('title', 'Nuevo Rol')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Nuevo Rol</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
      
      </div>
      
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        @include('flash::message')
        <form class="needs-validation form-horizontal" novalidate action="{{ route('rol.store') }}" method="POST" enctype="multipart/form-data" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    {{ csrf_field() }}
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Nombre Rol:</label></div>
              <div class="col-md-6"><input name="des_rol" id="des_rol" class="form-control" type="text" value="" required>
                <div class="invalid-feedback">
                    Introduzca el <i>Nombre</i> entre 2 y 100 letras 
                </div>
              </div>
            </div>
          </div>
          <br>
          <div class="">
            <div class="row" >
              <div class="col-md-3"><h5>Funcionalidad:</h5></div>
              <div class="col-md-6"></div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Home:</label></div>
              <div class="col-md-6">
                <input id="op1" type="checkbox"  name="op1" class="form-check-input" value="d" >
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Usuarios:</label></div>
              <div class="col-md-8">
                <input id="op2" type="checkbox"  name="op2" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Roles:</label></div>
              <div class="col-md-8">
                <input id="op3" type="checkbox"  name="op3" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Reglas:</label></div>
              <div class="col-md-8">
                <input id="op4" type="checkbox"  name="op4" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Areas:</label></div>
              <div class="col-md-8">
                <input id="op5" type="checkbox"  name="op5" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Profesionales:</label></div>
              <div class="col-md-8">
                <input id="op6" type="checkbox"  name="op6" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos:</label></div>
              <div class="col-md-8">
                <input id="op7" type="checkbox"  name="op7" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos Asignados:</label></div>
              <div class="col-md-8">
                <input id="op8" type="checkbox"  name="op8" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos Habilitados:</label></div>
              <div class="col-md-8">
                <input id="op9" type="checkbox"  name="op9" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos a Evaluar:</label></div>
              <div class="col-md-8">
                <input id="op10" type="checkbox"  name="op10" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Competencias:</label></div>
              <div class="col-md-8">
                <input id="op11" type="checkbox"  name="op11" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Intereses:</label></div>
              <div class="col-md-8">
                <input id="op12" type="checkbox"  name="op12" class="form-check-input" value="d" >
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-5 control-label"></label>
              <input type="submit" class="btn btn-primary btnColor" value="Guardar">

              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
          </div>
        </form>
      </div>
      <div class="col-md-2">
        
      </div>
  </div>
</div>

@endsection