@extends('master')
@section('title', 'Editar Rol')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Editar Rol</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
        
      </div>
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        <h4>Información del Rol</h4> 
        
        <form class="needs-validation" novalidate action="{{ route('rol.update',$rol->id )}}" method ="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

        
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Nombre Rol:</label></div>
              <div class="col-md-8">  
                <input name="des_rol" id="des_rol" class="form-control" type="text" value="{{ $rol->des_rol }}" required>
                <div class="invalid-feedback">
                    Introduzca el <i>Nombre</i> entre 2 y 100 letras 
                </div>
              </div>
            </div>
          </div>

          <div class="">
            <div class="row" >
              <div class="col-md-3"><h5>Funcionalidad:</h5></div>
              <div class="col-md-6"></div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Home:</label></div>
              <div class="col-md-6">
                @if($arreglo[0] == 0)
                  <input id="op1" type="checkbox"  name="op1" class="form-check-input" value="d" checked>
                @else
                  <input id="op1" type="checkbox"  name="op1" class="form-check-input" value="d" >
                @endif
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Profesionales:</label></div>
              <div class="col-md-8">
                @if($arreglo[1] == 0)
                  <input id="op2" type="checkbox"  name="op2" class="form-check-input" value="d" checked >
                @else
                  <input id="op2" type="checkbox"  name="op2" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Usuarios:</label></div>
              <div class="col-md-8">
                @if($arreglo[2] == 0)
                  <input id="op3" type="checkbox"  name="op3" class="form-check-input" value="d" checked>
                @else
                  <input id="op3" type="checkbox"  name="op3" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Roles:</label></div>
              <div class="col-md-8">
                @if($arreglo[3] == 0)
                  <input id="op4" type="checkbox"  name="op4" class="form-check-input" value="d" checked >
                @else
                  <input id="op4" type="checkbox"  name="op4" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Reglas:</label></div>
              <div class="col-md-8">
                @if($arreglo[4] == 0)
                  <input id="op5" type="checkbox"  name="op5" class="form-check-input" value="d" checked >
                @else
                  <input id="op5" type="checkbox"  name="op5" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Areas:</label></div>
              <div class="col-md-8">
                @if($arreglo[5] == 0)
                  <input id="op6" type="checkbox"  name="op6" class="form-check-input" value="d" checked >
                @else
                  <input id="op6" type="checkbox"  name="op6" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos:</label></div>
              <div class="col-md-8">
                @if($arreglo[6] == 0)
                  <input id="op7" type="checkbox"  name="op7" class="form-check-input" value="d" checked >
                @else
                  <input id="op7" type="checkbox"  name="op7" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos Asignados:</label></div>
              <div class="col-md-8">
                @if($arreglo[7] == 0)
                  <input id="op8" type="checkbox"  name="op8" class="form-check-input" value="d" checked >
                @else
                  <input id="op8" type="checkbox"  name="op8" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos Habilitados:</label></div>
              <div class="col-md-8">
                @if($arreglo[8] == 0)
                  <input id="op9" type="checkbox"  name="op9" class="form-check-input" value="d" checked >
                @else
                  <input id="op9" type="checkbox"  name="op9" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Competencias:</label></div>
              <div class="col-md-8">
                @if($arreglo[9] == 0)
                  <input id="op10" type="checkbox"  name="op10" class="form-check-input" value="d" checked >
                @else
                  <input id="op10" type="checkbox"  name="op10" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Intereses:</label></div>
              <div class="col-md-8">
                @if($arreglo[10] == 0)
                  <input id="op11" type="checkbox"  name="op11" class="form-check-input" value="d" checked >
                @else
                  <input id="op11" type="checkbox"  name="op11" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Proyectos a Evaluar:</label></div>
              <div class="col-md-8">
                @if($arreglo[11] == 0)
                  <input id="op12" type="checkbox"  name="op12" class="form-check-input" value="d" checked >
                @else
                  <input id="op12" type="checkbox"  name="op12" class="form-check-input" value="d" >
                @endif
                
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="submit" class="btn btn-primary btnColor" value="Guardar Cambios">
              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
            </div>
          </div>

        </form>
      </div>
      <div class="col-md-2">
        
      </div>
  </div>
</div>

@endsection