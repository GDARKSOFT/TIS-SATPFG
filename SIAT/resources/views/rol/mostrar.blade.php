@extends('master')
@section('title', 'Mostrar Rol')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Información del Rol</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
        
      </div>
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
      
        <form  action="" method ="POST" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Nombre Rol:</div>
              <div class="col-md-6">{{ $rol->des_rol }}</div>
            </div>
          </div>
          
          <div class="">
            <div class="row" >
              <div class="col-md-3"><h5>Funcionalidades:</h5></div>
              <div class="col-md-6"></div>
            </div>
          </div>
          @foreach($funciones as $fun)
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"></div>
              <div class="col-md-6">{{ $fun->des_fun }}</div>
            </div>
          </div>
          @endforeach
          <br>
          <br>
          </div>
      
      <div class="estiloCard">
          <a href="{{ route('rol.index')}}" class="btn btnColor btnCard">Atrás</a>
          <br>
      </div>
      
  </div>
</div>

@endsection