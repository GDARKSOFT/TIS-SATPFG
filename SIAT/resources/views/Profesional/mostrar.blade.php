@extends('master')
@section('title', 'Mostrar Profesional')
@section('content')
@extends('cabecera-con-sesion-usuario') 
<div class="container">
    <h3>Profesional</h3>
    <hr>
    <div class="row">
        <div class="col-md-4"></div>
        <br>
        <form  action="{{ route('profesionales.update',$profesional->id_prof)}}" method ="POST" enctype="multipart/form-data">
                        <H4 class="">Informacion del Profesional:  <i>{{ $profesional->trab_prof}}</i></H4>
                        <br><br>        
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Carnet de Identidad:</b></div>
                                <div class="col-md-6">{{ $profesional->ci_prof }}</div>
                            </div>
                        </div><br>  
                        @if($profesional->trab_prof == 'Interno')
                        <div class="container">
                            <div class="row">
                            <div class="col-md-6"><b>Código SIS:</b></div>
                            <div class="col-md-6">{{ $profesional->cod_prof }}</div>
                            </div>
                        </div><br>
                        @endif
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Nombre:</b></div>
                                <div class="col-md-6" >{{$profesional->nom_prof}}</div>
                            </div>
                        </div><br>  
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Apellido Paterno:</b></div>
                                <div class="col-md-6">{{$profesional->app_prof}}</div>
                            </div>
                        </div><br>  
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Apellido Materno:</b></div>
                                <div class="col-md-6">{{$profesional->apm_prof}}</div>
                            </div>
                        </div><br>    
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Telefono:</b></div>
                                <div class="col-md-6">{{$profesional->tel_prof}}</div>
                            </div>
                        </div><br>  
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Dirección:</b></div>
                                <div class="col-md-6">{{$profesional->dir_prof}}</div>
                            </div>
                        </div><br>  
                        <div class="container">
                            <div class="row" >
                                <div class="col-md-6"><b>Correo Electronico:</b></div>
                                <div class="col-md-6">{{$profesional->cor_prof}}</div>
                            </div>
                        </div><br>  
                <div class="estiloCard">
                    <a href="{{ route('profesionales.index')}}" class="btn btnColor btnCardV">Atrás</a>
                    <br>
                </div>
        </form>
    </div>
</div>
<br>
@endsection