@extends('master')
@section('title', 'Áreas de Competencia')
@section('content')
	@extends('cabecera-con-sesion-usuario')
	<h2  class="title"><P>REGISTRO DE ÁREAS DE COMPETENCIA</P></h2>

	<div class="content">		
		{!!Form::open(['route' => 'guardar.competencias','method' => 'post'])!!}
		{{  csrf_field() }}
		<div class="container">			
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4"><h4>Áreas</h4></div>
				<div class="col-md-4"><h4>Subáreas</h4></div>
				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
			</div>		
			<div class="row">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					{!! Form::select('areas',$areas,null,['id'=>'areas','class'=>'form-control','placeholder'=>'Seleccione']) !!}	
				</div>
				<div class="col-md-4">
					{!! Form::select('subareas',[],null,['id'=>'subareas','class'=>'form-control']) !!}
				</div>
				<div class="col-md-2">
					<button type="submit" class="btn btn-primary changeColorBtn btnColor">Guardar</button>	
				</div>			
				<div class="col-md-1"></div>
			</div>
		</div>
		{{ Form::close() }}
	</div>
	<div class="container">
		@include('flash::message')
		<div class="containerTable">
		<table id="tablaCompetencias" class="table table-hover table-table-bordered">
			<thead class="headTableColor">
				<tr class="titleSinNegrilla">
					<th scope="col">Id</th>
					<th scope="col">Area</th>
				</tr>
			</thead>
		</table>
		</div>
	</div>
@endsection
@section('scripts')
    <script type="text/javascript">
          $(function() {
          $('#tablaCompetencias').DataTable({
              processing: true,
              serverSide: true,
              ajax: "{{route('datatable.competencia')}}",
              columns: [
                  { data: 'id', name: 'id' },
                  { data: 'nom_area', name: 'nom_area'},
              ],
              language: esp,
              searching: false, 
              paging: false,
              info:false,
          });
        });
    </script>
@endsection

