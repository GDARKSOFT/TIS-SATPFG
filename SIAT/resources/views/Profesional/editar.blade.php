@extends('master')
@section('title', 'Editar Profesional')
@section('content')
@extends('cabecera-con-sesion-usuario') 
<br>
<div class="card miCard">
    <h3 class = "titCard" align="center">Formulario Editar Profesional <i>{{ $profesional->trab_prof}}</i></h3>
    <hr>
    <div class="card-body">
        <!-- <h3 class = "titCard" align="center"></h3> -->
        <br>
        <form class="needs-validation" novalidate action="{{ route('profesionales.update',$profesional->id_prof)}}" method ="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                        @if($profesional->trab_prof == 'Interno')
                        <div class="form-group">
                            <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-2"><label class="control-label">Código SIS:</label></div>
                            <div class="col-md-6">
                                <input name="cod_prof" id="cod_prof" class="form-control" type="text" pattern="^([0-9]+){7,20}$" minlength="8" maxlength="15" value="{{ $profesional->cod_prof}}">
                                <div class="invalid-feedback">
                                    Introduzca el <i>Código SIS</i> entre 8 y 15 caracteres  
                                </div>
                            </div>
                            <div class="col-md-2"></div>
                            </div>
                        </div>
                        @endif
                        
                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2"></div>
                              <div class="col-md-2"><label class="control-label" align="left">Carnet de Identidad:</label></div>
                              <div class="col-md-6">
                                    <input name="ci_prof" type="text" class="form-control" id="ci_prof" placeholder="Carnet de Identidad" pattern="^[A-Z]*?{1,3}\d{6,10}$" minlength="6" maxlength="50" value="{{ $profesional->ci_prof }}" required>
                                    <div class="invalid-feedback">
                                            Introduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres  
                                    </div>
                              </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2"></div>
                              <div class="col-md-2"><label class="control-label" align="left">Nombre:</label></div>
                              <div class="col-md-6">
                                    <input name="nom_prof" type="text" class="form-control" id="nom_prof" placeholder="Nombre" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100"  value="{{ $profesional->nom_prof }}" required>
                                    <div class="invalid-feedback">
                                            Introduzca el <i>Nombre</i> entre 2 y 100 letras
                                    </div>
                              </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2"></div>
                              <div class="col-md-2"><label class="control-label" align="left">Apellido Paterno:</label></div>
                              <div class="col-md-6">
                                    <input name="app_prof" type="text" class="form-control" id="app_prof" placeholder="Apellido Paterno" value="{{$profesional->app_prof}}"  minlength="2" maxlength="100">
                                    <div class="invalid-feedback">
                                            Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
                                    </div>
                              </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2"></div>
                              <div class="col-md-2"><label class="control-label" align="left">Apellido Materno:</label></div>
                              <div class="col-md-6">
                                    <input name="apm_prof" type="text" class="form-control" id="apm_prof" placeholder="Apellido Materno" value="{{ $profesional->apm_prof }}"  minlength="2" maxlength="100">
                                    <div class="invalid-feedback">
                                            Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
                                    </div>
                              </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2"></div>
                              <div class="col-md-2"><label class="control-label" align="left">Teléfono:</label></div>
                              <div class="col-md-6">
                                    <input name="tel_prof" type="text" class="form-control" id="tel_prof" placeholder="Telefono" value="{{$profesional->tel_prof}}" pattern="^([0-9]+){7,20}$" minlength="7" required>
                                    <div class="invalid-feedback">
                                            Introduzca el <i>Telefono</i> entre 7 y 20 numeros
                                    </div>
                              </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2 "></div>
                              <div class="col-md-2 "><label class="control-label" align="left">Dirección:</label></div>
                              <div class="col-md-6 ">
                                    <input name="dir_prof" type="text" class="form-control" id="dir_prof" placeholder="Direccion" required value="{{$profesional->dir_prof}}" minlength="7">
                                    <div class="invalid-feedback">
                                            Introduzca la <i>Direccion</i> entre 10 y 100 caracteres
                                    </div>
                              </div>
                              <div class="col-md-2 "></div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="row" >
                              <div class="col-md-2 "></div>
                              <div class="col-md-2 "><label class="control-label" align="left">Correo Electrónico:</label></div>
                              <div class="col-md-6 ">
                                    <input name="cor_prof" type="email" class="form-control" id="cor_prof" placeholder="Correo Electronico" value="{{$profesional->cor_prof}}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                    <div class="invalid-feedback">
                                            Introduzca una dirección de <i>Correo Electronico</i> válida
                                    </div>
                             </div>
                              <div class="col-md-2"></div>
                            </div>
                        </div>
                        
                <div class="estiloCard">
                    <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                    <button class="btn btnColor btnCard" type="submit">Guardar Cambios</button>
                    <br>
                </div>
        </form>
    </div>
</div>
<br>
@endsection