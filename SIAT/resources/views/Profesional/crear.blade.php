@extends('master')
@section('title', 'Nuevo Profesional')
@section('content')
@extends('cabecera-con-sesion-usuario') 
<div class="card miCard">
    <div class="card-body">
        <h3 class = "titCard" align="center">Nuevo Profesional</h3>
        @include('flash::message')
        <hr>
        <br>
        <form class="needs-validation" novalidate action="{{ route('profesionales.store') }}" method ="POST" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3">
                                <input name="ci_prof" type="text" class="form-control" id="ci_prof" placeholder="Carnet de Identidad" pattern="^[A-Z]*?{1,3}\d{6,10}$" minlength="6" maxlength="50" required>
                                <div class="invalid-feedback">
                                    Introduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3"> 
                                <input name="nom_prof" type="text" class="form-control" id="nom_prof" placeholder="Nombre" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                                <div class="invalid-feedback">
                                    Introduzca el <i>Nombre</i> entre 2 y 100 letras 
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-2">
                                <input name="app_prof" type="text" class="form-control" id="app_prof" placeholder="Apellido Paterno" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100">
                                <div class="invalid-feedback">
                                    Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3">
                                <input name="apm_prof" type="text" class="form-control" id="apm_prof" placeholder="Apellido Materno" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                                <div class="invalid-feedback">
                                    Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3">
                                <input name="tel_prof" type="text" class="form-control" id="tel_prof" placeholder="Teléfono" pattern="^([0-9]+){7,20}$">
                                <div class="invalid-feedback">
                                    Introduzca el <i>Telefono</i> entre 7 y 20 numeros
                            </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3">
                                <input name="dir_prof" type="text" class="form-control" id="dir_prof" placeholder="Dirección" pattern="^[a-z ñ.áéíóúäëïöü\'-]+(\s*[a-z ñ.áéíóúäëïöü\'-]*)*[a-z ñ.áéíóúäëïöü\'-]+$" maxlength="100">
                                <div class="invalid-feedback">
                                    Introduzca la <i>Direccion</i> entre 10 y 100 caracteres
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                        </div>
                        <div class="form-row">
                            <div class="col-md-2 mb-3"></div>
                            <div class="form-group col-md-8 mb-3">
                                <input name="cor_prof" type="email" class="form-control" id="cor_prof" placeholder="Correo Electrónico" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                                <div class="invalid-feedback">
                                    Introduzca una dirección de <i>Correo Electronico</i> válida
                                </div>
                            </div>
                            <div class="col-md-2 mb-3"></div>
                         </div>    
                         <div class="form-row">
                            <div class="col-md-2 mb-4"></div>
                            <div class="col-md-2 mb-4">
                                <label for="trab_prof">Tipo Trabajo: </label>
                            </div>
                            <div class="form-group col-md-6 mb-4">
                                <select name="trab_prof" id="trab_prof" class="form-control custom-select" required>
                                    <option disabled selected hidden></option>
                                    <option value="Interno">Interno</option>
                                    <option value="Externo">Externo</option>
                                </select>
                                <div class="invalid-feedback">
                                    Seleccione una opcion
                                </div>
                            </div>
                            <div class="col-md-2 mb-4"></div>
                         </div>            
                <div class="estiloCard">
                    <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                    <button class="btn btnColor btnCard" type="submit" data-toggle="modal" data-target="#mensaje" value ="1">Guardar</button>
                    <br>
                </div> 
        </form>
         
    </div>
</div>
<br>
@endsection