@extends('master')
@section('title', 'Inicio Sesión')
@section('content')
<div class="container">
    <div class="cuerpo centrar-items">
        <div class="col-md-offset-3">
            <div class="myPanel">
                <div class="panel-heading">
                    <h3>Bienvenido al Sistema</h3>
                    <hr>
                </div>

                <div class="panel-body">
                     @if(count($errors) > 0)
                        <div class="alert alert-danger alertLogin" role="alert">
                                @foreach($errors -> all() as $error)
                                   {{ $error }}
                                @endforeach
                        </div>
                    @endif
                    <div class="col-lg-12">
                         
                        <form class="formato-login needs-validation" novalidate role="form" method="POST" action="{{ route('login') }}">
                            {{ csrf_field() }}  
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

                                <div class="input-group estiloCardOtro">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                        <input id="email" type="email" class="form-control" name="email" tabindex="1" value="{{ old('email') }}" placeholder="Email de Usuario" pattern="[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required autofocus>
                                        <div class="invalid-feedback">
                                             Introduzca una dirección de correo válida
                                        </div>
                                        
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="input-group estiloCardOtro">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text" id="iconEye">
                                            <i id="icon" class="fas fa-eye"></i>                              
                                        </div>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password" tabindex="2" placeholder="Contraseña" required>
                                    <div class="invalid-feedback">
                                        Introduzca su contraseña
                                    </div>
                                </div>
                            </div>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <div class="row">
                                        <div class="col-md-6"  style="padding-top:5px">
                                            <input name:"remember" {{old('remember')? 'checked' : ''}} type="checkbox" aria-label="Checkbox for following text input">
                                            <span> &nbsp Recordarme</span>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-5">
                                            <a class="btn btn-link" href="{{ url('/password/reset') }}"> Olvidaste tu contraseña?</a> 
                                        </div>
                                    </div>  
                                </div> 
                            </div>
                            
                            
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-default" type="reset" onclick="history.back()" style="width: 100%;">Cancelar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <input id="iniciarSesion" class="btn btn-primary btnFormatIS changeColorBtn btnColor" data-submit="...Enviando" type="submit"  value="Iniciar Sesión" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection