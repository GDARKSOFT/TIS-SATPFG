@extends('master')
@section('title', 'Restablecer contraseña')
@section('content')
<div class="container">
    <div class="cuerpo centrar-items">
        <div class="col-md-offset-3">
            <div class="myPanel">
                <div class="panel-heading">
                <h3>Restablecer contraseña</h1>
                <hr>
                </div>
                
                <div class="panel-body">
                    <div class="col-lg-12">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        <span>
                                            {{$error}}
                                        </span>
                                    @endforeach

                            </div>
                            
                        @endif 

                        <form class="formato-login needs-validation" novalidate method="POST" action="{{ route('password.email') }}">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <div class="input-group estiloCardOtro">
                                     <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-user"></i>
                                        </div>
                                    </div>
                                    <input id="email" type="email" class="form-control" name="email" tabindex="1" value="{{ old('email') }}" placeholder="Email de Usuario" pattern="[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?" required autofocus>
                                    <div class="invalid-feedback">
                                             Introduzca una dirección de correo válida
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-default" type="reset" onclick="history.back()" style="width: 100%;">Cancelar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <input type="submit" class="btn btn-primary btnColor" value="Restablecer Contraseña" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                            <br>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
