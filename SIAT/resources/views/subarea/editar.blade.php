@extends('master')
@section('title', 'Editar Subárea')
@section('content')
@extends('cabecera-con-sesion-usuario')

@if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach($errors -> all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
            <div class="card miCard marginDivUp">
                
                <div class="card-body">
                     <h3 class = "titCard" align="left"></h3>

                        <form class="needs-validation" novalidate action="{{ route('subareas.update', ['id' => $area->id, 'idSub' => $subarea->id])}}" method="POST">
                                    <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}

                            <div class="form-group estiloCard">
                                
                                <input class="form-control" name="nom_subar" type="text" id="nom_subar" value="{{ $subarea->nom_area }}" placeholder="Nombre del Subárea" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                                <div class="invalid-feedback">
                                    Introduzca entre 5 y 100 letras
                                </div>

                            </div>

                            <div class="form-group estiloCard">
                                <textarea rows="4" name="des_subar" class="form-control" placeholder="Descripcion del Subárea de conocimiento">{{ $subarea->des_area }}</textarea>
                            </div>

                            <div class="estiloCard">
                                
                                <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                                <button type="submit" class="btn btnColor btnCard">Guardar Cambios</button>

                            </div>
                        </form>
                     </div>
                    
                </div>
            

@endsection