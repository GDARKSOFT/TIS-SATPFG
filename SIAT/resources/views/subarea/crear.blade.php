@extends('master')
@section('title', 'Nueva Subárea')
@section('content')
@extends('cabecera-con-sesion-usuario') 
            <div class="card miCard marginDivUp">
                <div class="card-body">
                     <h3 class = "titCard" align="center">Nueva Subárea</h3>
                     @include('flash::message')
                     <div>    
                        <form class="needs-validation" novalidate action="{{route('subareas.store', $area->id)}}" method ="POST" class="needs-validation" novalidate  enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            <div class="form-group estiloCard">

                                <input name="nom_subar" type="text" class="form-control" placeholder="Nombre del Subárea" value = "{{old('nom_subar')}}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="5" required>
                                
                                <div class="invalid-feedback">
                                    Introduzca entre 5 y 100 letras
                                </div>
                            </div>

                            <div class="form-group estiloCard">
                                <textarea rows="4" name="des_subar" class="form-control" placeholder="Descripcion del Subárea de conocimiento">{{old('des_subar')}}</textarea>
                            </div>

                            <div class="estiloCard">
                                <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                                <button id="guardar" class="btn btnColor btnCard" type="submit" data-toggle="modal" data-target="#mensaje" value ="1">Guardar</button>

                                
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <br>
@endsection
