@extends('master')
@section('title', 'Proyectos Asignados')
@section('content')
@extends('cabecera-con-sesion-usuario')

<h2 class="title">
    <p>PROYECTOS ASIGNADOS</p>
</h2>
<div class="container">
    @include('flash::message')
    <div class="containerTable">
        <table id="tablaProyectosAsignados" class="table table-hover table-table-bordered">
            <thead class="headTableColor">
                <tr class="titleSinNegrilla">
                    <th scope="col">Id</th>
                    <th scope="col">Título Proyecto</th>
                    <th scope="col">Nombre Estudiante</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
        </table>
    </div>
    <br />
    <div class="btnFormat">
        <div class="row" style="text-align: center">
            <div class="col-md-2">
                <button type="button" class="btn btn-primary changeColorBtn btnColor" onclick="history.back()">Atrás</button>
            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
<script type="text/javascript">
		$(function() {
	  $('#tablaProyectosAsignados').DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: "{{route('datatable.proyectosAsignadosProfesional')}}",
	      columns: [
	          { data: 'id', name: 'id' },
	          { data: 'tit_proy', name: 'tit_proy' },
	          { data: 'nom_est', name: 'nom_est' },
	          { data: 'action', name: 'action', orderable: false, searchable: false}
	      ],
	      language: esp
	  });
	});
</script>
@endsection