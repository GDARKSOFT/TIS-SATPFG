@extends('master')
@section('title', 'Perfil')
@section('content')
@extends('cabecera-con-sesion-usuario')
  <div class="container">
    <h3>Editar Perfil</h3>
    <hr>
  	<br>
	<div class="row">
      <!-- left column -->
      <div class="col-md-5">
        <div class="text-center">
        <img src="{{ asset('uploads/avatars/'.Auth::user()->avatar) }}" class="avatar img-circle fotoPerfil" alt="avatar" style="border-radius: 50%;">
          <h6>Actualice su foto...</h6>
          <form enctype="multipart/form-data" action="{{ route('perfil.update.foto')}}" method="POST">
            <input type="file" name="avatar" style="width: 400px;" class="form-control">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <br>
            <input type="submit" class="pull-center btn btn-primary btnColor" value="Cambiar Foto">
          </form>
        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-7 personal-info">
        
        <h3>Información Personal <i>{{$profesional->trab_prof}}</i></h3>
        
        <form class="needs-validation" novalidate action="{{ route('perfil.update',$profesional->id_prof)}}" method ="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
          @if($profesional->trab_prof == 'Interno')
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Código SIS:</label></div>
              <div class="col-md-6"><input name="cod_prof" id="cod_prof" class="form-control" type="text" pattern="^([0-9]+){7,20}$" minlength="8" maxlength="15" value="{{$profesional->cod_prof}}">
                <div class="invalid-feedback">
                      Introduzca el <i>Código SIS</i> entre 8 y 15 caracteres  
                </div>
            </div>
            </div>
          </div>
          @endif
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Carnet de Identidad:</label></div>
              <div class="col-md-6"><input name="ci_prof" id="ci_prof" class="form-control" type="text" pattern="^[A-Z]*?{1,3}\d{6,10}$" minlength="6" value="{{$profesional->ci_prof}}" required>
                  <div class="invalid-feedback">
                      Introduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres  
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Nombre:</label></div>
              <div class="col-md-6"><input name="nom_prof" class="form-control" type="text" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" value="{{ $profesional->nom_prof }}" required>
                  <div class="invalid-feedback">
                      Introduzca el <i>Nombre</i> entre 2 y 100 letras
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Paterno:</label></div>
              <div class="col-md-6"><input name="app_prof" class="form-control" type="text" value="{{$profesional->app_prof}}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100">
                <div class="invalid-feedback">
                    Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Materno:</label></div>
              <div class="col-md-6"><input name="apm_prof" class="form-control" type="text" value="{{ $profesional->apm_prof }}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                  <div class="invalid-feedback">
                    Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
                  </div>
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Teléfono:</label></div>
              <div class="col-md-6"><input name="tel_prof" class="form-control" type="text" value="{{$profesional->tel_prof}}" pattern="^([0-9]+){7,20}$" minlength="7">
                  <div class="invalid-feedback">
                        Introduzca el <i>Telefono</i> entre 7 y 20 numeros
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Dirección:</label></div>
              <div class="col-md-6"><input name="dir_prof" class="form-control" type="text" value="{{$profesional->dir_prof}}" pattern="^[a-z ñ.áéíóúäëïöü\'-]+(\s*[a-z ñ.áéíóúäëïöü\'-]*)*[a-z ñ.áéíóúäëïöü\'-]+$" minlength="10" maxlength="100">
                  <div class="invalid-feedback">
                        Introduzca la <i>Direccion</i> entre 10 y 100 caracteres
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Correo Electrónico:</label></div>
              <div class="col-md-6"><input name="cor_prof" class="form-control" type="text" value="{{$profesional->cor_prof}}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                  <div class="invalid-feedback">
                        Introduzca una dirección de <i>Correo Electronico</i> válida
                  </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-6 control-label"></label>
            <div class="col-md-8">
              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              <input type="submit" class="btn btn-primary btnColor" value="Guardar Cambios">
              
            </div>
          </div>

        </form>
      </div>
  </div>
</div>
@endsection