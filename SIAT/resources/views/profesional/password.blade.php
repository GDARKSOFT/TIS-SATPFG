@extends('master')
@section('title', 'Cambiar Contraseña')
@section('content')
<div class="container">
    <div class="cuerpo centrar-items">
        <div class="col-md-offset-3">
            <div class="myPanel">
                        <div class="panel-heading">
                            <h3>Cambiar Contraseña</h3>
                            <hr>
                            <br>
                        </div>
                        @include('flash::message')
                        <div class="panel-body">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                    @foreach($errors->all() as $error)
                                        <span>
                                            {{$error}}
                                        </span>
                                    @endforeach
                            </div>
                            
                        @endif 
                          
                            <form class="form-horizontal marginFieldReset needs-validation" novalidate role="form" method="POST" action="{{url('User/updatepassword')}}">
                                {{ csrf_field() }}
                                <div class="form-group{{ $errors->has('mypassword') ? ' has-error' : '' }}">
                                     <div class="col-md-12">   
                                        <div class="input-group estiloCardOtro">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="iconEyeActual">
                                                    <i id="iconActual" class="fa fa-eye"></i>
                                                </div>
                                            </div>
                                            <input  id="mypassword" type="password" name="mypassword" class="form-control" placeholder="Contraseña Actual" required>
                                            <!-- <div class="text-danger">{{$errors->first('mypassword')}}</div> -->
                                            <div class="invalid-feedback">
                                                        Introduzca entre 6 y 15 caracteres (al menos una letra minúscula y al menos un número).
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <div class="input-group estiloCardOtro">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="iconEye">
                                                    <i id="icon" class="fa fa-eye"></i>
                                                </div>
                                            </div>
                                            <input id="password" type="password" class="form-control" placeholder="Nueva contraseña" name="password" pattern="^(?=.*[\d])(?=.*[a-zA-Z])[\w\.*\W.*]{6,15}$" required>
                                            <div class="invalid-feedback">
                                                Introduzca entre 6 y 15 caracteres (al menos una letra minúscula y al menos un número).
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <div class="col-md-12">
                                        <div class="input-group estiloCardOtro">
                                            <div class="input-group-prepend">
                                                <div class="input-group-text" id="iconEyeConfirm">
                                                    <i id="iconConfirm" class="fa fa-eye"></i>
                                                </div>
                                            </div>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="Confirme contraseña" pattern="^(?=.*[\d])(?=.*[a-zA-Z])[\w\.*\W.*]{6,15}$" required>
                                            <div class="invalid-feedback">
                                                Vuelva a introducir la contraseña
                                            </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                <div class="row">
                                    <div class="col-md-6">
                                        <button class="btn btn-default" type="reset" onclick="history.back()" style="width: 100%;">Cancelar</button>
                                    </div>
                                    <div class="col-md-6">
                                        <input id="iniciarSesion" class="btn btn-primary btnFormatIS changeColorBtn btnColor" data-submit="...Enviando" type="submit"  value="Cambiar Contraseña" style="width: 100%;">
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
            </div>
        </div>
    </div>
</div>
@endsection
