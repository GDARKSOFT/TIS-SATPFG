@extends('master')
@section('title', 'Editar Usuario')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Editar Usuario</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
        
      </div>
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        <h4>Información Personal</h4> 
        
        <form class="needs-validation" novalidate action="{{ route('usuario.update',$iduser )}}" method ="POST" enctype="multipart/form-data">
            <input type="hidden" name="_method" value="PUT">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

          <div class="form-group">
            <label class="col-lg-6 control-label">Carnet de Identidad:</label>
            <div class="col-lg-8">
              <input name="ci_user" id="ci_user" class="form-control" type="text" value="{{ $user->ci_user }}"pattern="^[A-Z]*?{1,3}\d{6,10}$" minlength="6" maxlength="50" required>
              <div class="invalid-feedback">
                    Introduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres
              </div>
          </div>
          </div>
          <div class="form-group">
            <label class="col-lg-6 control-label">Nombre:</label>
            <div class="col-lg-8">
              <input name="nom_user" id="nom_user" class="form-control" type="text" value="{{ $user->nom_user }}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
              <div class="invalid-feedback">
                  Introduzca el <i>Nombre</i> entre 2 y 100 letras 
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-6 control-label">Apellido Paterno:</label>
            <div class="col-lg-8">
              <input name="app_user" id="app_user" class="form-control" type="text" value="{{ $user->app_user }}"pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100">
              <div class="invalid-feedback">
                  Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-lg-6 control-label">Apellido Materno:</label>
            <div class="col-lg-8">
              <input name="apm_user" id="apm_user" class="form-control" type="text" value="{{ $user->apm_user }}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
              <div class="invalid-feedback">
                  Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
              </div>
            </div>
          </div>
          
          
          <div class="form-group">
            <label class="col-lg-3 control-label">Tipo:</label>
            <div class="col-lg-8">
              
                <select id="user_rol" name="user_rol" class="form-control">
                  @foreach($roles as $roll)
                    @if($roll->id == $rol)
                      <option value="{{$roll->des_rol}}" selected>{{$roll->des_rol}}</option>
                    @else
                      <option value="{{$roll->des_rol}}">{{$roll->des_rol}}</option>
                    @endif
                  @endforeach
                </select>
            
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-lg-6 control-label">Email:</label>
            <div class="col-lg-8">
              <input name="email" id="email" class="form-control" type="email" value="{{ $user->email }}" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
              <div class="invalid-feedback">
                  Introduzca una dirección de <i>Correo Electronico</i> válida
              </div>
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input type="submit" class="btn btn-primary btnColor" value="Guardar Cambios">
              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
            </div>
          </div>

        </form>
      </div>
      <div class="col-md-2">
        
      </div>
  </div>
</div>

@endsection