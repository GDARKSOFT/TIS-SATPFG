@extends('master')
@section('title', 'Nuevo Usuario')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Nuevo Usuario</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
      
      </div>
      
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        <h4>Información Personal</h4>
        <br>
        @include('flash::message')
        <form class="needs-validation form-horizontal" novalidate action="{{ route('usuario.store') }}" method="POST" enctype="multipart/form-data" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                    {{ csrf_field() }}
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Carnet de Identidad:</label></div>
              <div class="col-md-6"><input name="ci_user" id="ci_user" class="form-control" type="text" value="" pattern="^[A-Z]*?{1,3}\d{6,10}$" minlength="6" maxlength="50" required>
                <div class="invalid-feedback">
                    Introduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Nombre:</label></div>
              <div class="col-md-6"><input name="nom_user" id="nom_user" class="form-control" type="text" value="" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                <div class="invalid-feedback">
                  Introduzca el <i>Nombre</i> entre 2 y 100 letras 
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Apellido Paterno:</label></div>
              <div class="col-md-6"><input name="app_user" id="app_user" class="form-control" type="text" value="" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100">
                <div class="invalid-feedback">
                  Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Apellido Materno:</label></div>
              <div class="col-md-6"><input name="apm_user" id="apm_user" class="form-control" type="text" value="" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="2" maxlength="100" required>
                <div class="invalid-feedback">
                  Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Tipo:</label></div>
              <div class="col-md-6">
                  <select id="user_rol" name="user_rol" class="form-control">
                    @foreach($roles as $rol)
                    <option value="{{$rol->des_rol}}">{{$rol->des_rol}}</option>
                    @endforeach
                  </select></div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3"><label class="control-label">Correo Electrónico:</label></div>
              <div class="col-md-6"><input name="email" id="email" class="form-control" type="email" value="" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" required>
                <div class="invalid-feedback">
                  Introduzca una dirección de <i>Correo Electronico</i> válida
                </div>
              </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-5 control-label"></label>
              <input type="submit" class="btn btn-primary btnColor" value="Guardar">

              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
          </div>
        </form>
      </div>
      <div class="col-md-2">
        
      </div>
  </div>
</div>

@endsection