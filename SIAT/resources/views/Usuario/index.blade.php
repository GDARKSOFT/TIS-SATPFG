@extends('master')
@section('title', 'Usuarios')
@section('content')
@extends('cabecera-con-sesion-usuario')

<!-- Image and text -->
<h2  class="title">USUARIOS</h2>
	<div class="container">
		@include('flash::message')
		<div class = "containerTable">
			<table id="tablaUsuario" class="table table-hover table-bordered">
				<thead class="headTableColor">
				<tr>
					<th scope="col">Id</th>
                    <th scope="col">Nombre</th>
					<th scope="col">Correo</th>
					<th scope="col">Tipo</th>
					<th>Acciones</th>
					
				</tr>
				</thead>
			</table>
		</div>
		<br>
		
		<div class="btnFormat">
			<div class="row" style="text-align: center">
				<div class="col-md-2"><button type="button" class="btn btn-primary changeColorBtn btnColor" onclick="history.back()">Atrás</button></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
				<div class="col-md-2"><a href="{{ route('usuario.create') }}" class="btn btn-primary changeColorBtn btnColor">nuevo usuario</a></div>
			</div>
		</div>

	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		  $(function() {
		  $('#tablaUsuario').DataTable({
		      processing: true,
		      serverSide: true,
		      ajax: "{{route('datatable.usuarios')}}",
		      columns: [
		          { data: 'id', name: 'id' },
		          { data: 'nom_user', name: 'nom_user' },
		          { data: 'email', name: 'email' },
		          { data: 'roles', name: 'roles'},
		          { data: 'action', name: 'action', orderable: false, searchable: false }
		          
		      ],
		      language: esp
		  });
		});
	</script>
@endsection