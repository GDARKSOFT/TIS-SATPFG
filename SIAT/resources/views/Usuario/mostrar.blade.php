@extends('master')
@section('title', 'Mostrar Usuario')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Usuario</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
        
      </div>
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        <h4>Información Personal</h4>
        
        <form  action="{{ route('usuario.update',$user->id)}}" method ="POST" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Carnet de Identidad:</div>
              <div class="col-md-6">{{ $user->ci_user }}</div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Nombre:</div>
              <div class="col-md-6">{{ $user->nom_user }}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Apellido Paterno:</div>
              <div class="col-md-6">{{ $user->app_user }}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Apellido Materno:</div>
              <div class="col-md-6">{{ $user->apm_user }}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Tipo:</div>
              <div class="col-md-6">{{ $rol->des_rol }}</div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Email:</div>
              <div class="col-md-6">{{ $user->email }}</div>
            </div>
          </div>
        </form>
      </div>
      
      <div class="estiloCard">
          <a href="{{ route('usuario.index')}}" class="btn btnColor btnCard">Atrás</a>
          <br>
      </div>
  </div>
</div>

@endsection