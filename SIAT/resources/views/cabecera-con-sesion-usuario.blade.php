<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand" href="#">
    <img class="logo" src="{{ asset('images/logo.png') }}" width="" height="60" alt="">
  </a>
  <div class = "row marginR">
        <div class = "dropdown posIcon marginNotif">
          @if(Auth::user()->roles()->first()->id == 1  || Auth::user()->roles()->first()->id == 2 ||  Auth::user()->roles()->first()->id == 3)
          @else
                <button id="button" type="button" class=" pull-xs-right marginNotif btn dropdown-toggle btn-outline-light " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <a href=""><i class="fa fa-btn fa-bell btnNotif"></i></a>
                  <span class="caret"></span>
                </button>
                <!-- <div id="notifCount" class="notifTopbar"></div> -->
                @foreach(Auth::user()->unreadNotifications as $notification)
                  <div class="dropdown-menu menuDropNotif">
                    <a class="dropdown-item smallWord" href="#">
                      <!-- Message title and timestamp -->
                        <p><b>{{$notification->data['name']}}</b></p>
                                <!-- The message -->
                        <p>{{$notification->data['body']}}</p>
                    </a>
                    <div class="dropdown-divider"></div>
                  </div>
              @endforeach
          @endif
        </div>
       <div class="dropdown marginR">
        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="text-decoration:none; color: #000;">
          <img class="classImgDefault" src="{{ asset('uploads/avatars/'.Auth::user()->avatar) }}">
          <span class="caret"></span>
        </a>
        </button>
      
        <ul class="dropdown-menu menuDrop" role="menu">
            <li><a><center>{{ Auth::user()->nom_user }}<center></a></li > 
            <div class="dropdown-divider"></div>
          @if(Auth::user()->roles()->first()->id == 1)
            <li><a href="{{ route('admin.perfil') }}"><i class="fa fa-btn fa-user"></i> Ver Perfil</a></li>     
          @elseif(Auth::user()->roles()->first()->id == 2)
            <li><a href="{{ route('secretaria.perfil') }}"><i class="fa fa-btn fa-user"></i> Ver Perfil</a></li>    
          @elseif(Auth::user()->roles()->first()->id == 4)
            <li><a role="menuitem" href="{{ route('perfil.show')}}"><i class="fa fa-btn fa-user"></i>Ver Perfil</a></li>

          @else
            <li><a href="{{ route('consejero.perfil') }}"><i class="fa fa-btn fa-user"></i> Ver Perfil</a></li>   
          @endif
          <li><a href="{{ url('User/password') }}"><i class="fa fa-btn fa-lock"></i> Seguridad</a></li>
          <li><a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();"><i class="fas fa-sign-out-alt"></i> 
              Cerrar Sesión</li>
          </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
          </form>
          
        </ul>
      </div>
  </div>
</nav>
<div class="auxNav">
    <div class="topNav barraNav">
        <div class="modLetNav">
          <?php $funciones = Auth::user()->roles()->first()->funciones()->get();?>
          @foreach($funciones as $fun)
                 @if($fun->des_fun == 'Home') 
                    <a href="{{ url('/') }}"  aling ="center" ><i class="fas fa-home"></i></a>
                 @endif
                 @if($fun->des_fun == 'Profesionales') 
                    <a href="{{ url('/profesionales') }}"  aling ="center" >PROFESIONALES</a>
                 @endif
                 @if($fun->des_fun == 'Usuarios') 
                    <a href="{{ route('usuario.index') }}"  aling ="center" >USUARIOS</a>
                 @endif
                 @if($fun->des_fun == 'Roles') 
                    <a href="{{ route('rol.index') }}"  aling ="center" >ROLES</a>
                 @endif
                 @if($fun->des_fun == 'Reglas') 
                    <a href="{{ url('/reglas') }}"  aling ="center" >REGLAS</a>
                 @endif
                 @if($fun->des_fun == 'Areas') 
                    <a href="{{ url('/areas') }}"  aling ="center" >ÁREAS</a>
                 @endif
                 @if($fun->des_fun == 'Proyectos') 
                    <a href="{{ url('/proyectos') }}"  aling ="center">PROYECTOS</a>
                    
                 @endif
                 @if($fun->des_fun == 'Proyectos Asignados') 
                    <a href="{{ route('asignaciones.asignados') }}"  aling ="center" >PROY. ASIGNADOS</a>
                 @endif
                 @if($fun->des_fun == 'Proyectos Habilitados') 
                    <a href="{{route('asignacion.index')}}"  aling ="center">PROY. HABILITADOS</a>
                 @endif
                 @if($fun->des_fun == 'Competencias') 
                    <a href="{{ url('/competencias') }}"  aling ="center" >COMPETENCIAS</a>
                 @endif
                 @if($fun->des_fun == 'Intereses') 
                    <a href="{{ url('/interes') }}"  aling ="center" >INTERESES</a>
                 @endif
                 @if($fun->des_fun == 'Proyectos a Evaluar') 
                    <a href="{{ route('profesional.proyecto')}}"  aling ="center" >PROY. a EVALUAR</a>
                 @endif
          @endforeach
        </div>
    </div>
</div>
<br>     