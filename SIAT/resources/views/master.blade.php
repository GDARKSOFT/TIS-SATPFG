<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="shortcut icon" href="images/favicon.ico" type="image/ico"/>
	<title>@yield('title')</title>
	<link rel="stylesheet" href="{{ asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('css/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('css/select.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <script type="text/javascript" href="{{ asset('js/validacion.js') }}"></script>
</head>

<body class = "fondo ">
    @yield('content')
    <!-- Modal -->
    <div class="modal fade" id="modal-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Eliminar</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">                                                           
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <form class="delete-form" action="" method="POST">
                        <input type="hidden" name="_method" value="DELETE">
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-primary" value="Aceptar" style="margin-top: 17px"></button>
                    </form>
                </div>
            </div>
        </div>
    </div>   

	<footer class="text-center ">
        <div class="footer-below">
            <div class="">
                <div class="row">
                    <div class="col-lg-12">
                        Copyright &copy; GdarkSoft <?php echo date("Y"); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
	<script src="{{ asset('js/jquery.js') }}"></script>
    <script src="{{ asset('js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('js/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('js/dataTables.select.js') }}"></script>
	<script src="{{ asset('js/bootstrap.js') }}"></script>
    <script src="{{ asset('js/fontawesome-all.js') }}"></script>
    <script src="{{ asset('js/siat.js') }}"></script>    
    <script src="{{ asset('js/controller.js') }}"></script>
    <script src="{{ asset('js/dropdown.js') }}"></script>
    <script src="{{ asset('js/box.js') }}"></script>


    <script src="{{ asset('js/dataTablesbuttonsmin.js') }}"></script>
    <script src="{{ asset('js/jszipmin.js') }}"></script>
    <script src="{{ asset('js/pdfmakemin.js') }}"></script>
    <script src="{{ asset('js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('js/buttonshtml5min.js') }}"></script>
    <script src="{{ asset('js/buttonsprintmin.js') }}"></script>
    @yield('scripts')
</body>
</html>