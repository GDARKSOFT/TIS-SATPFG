@extends('master')
@section('title', 'Asignaci�n de Tribunales')
@section('content')
@extends('cabecera-con-sesion-usuario')

<div class="container minHeight">
    <h2  class="title"><P>TRIBUNALES</P></h2>
    @include('flash::message')
	<div class="containerTable">
	<table id="tablaAsignar" class="table table-hover table-table-bordered">
        <thead class="headTableColor">
            <tr class="titleSinNegrilla">
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">N� Proyectos</th>
                <th scope="col">Acciones</th>
            </tr>
        </thead>
	</table>
    <button type="submit" class="btn btn-primary changeColorBtn btnColor">Asignar</button>	
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
          $(function() {
          $('#tablaAsignar').DataTable({
              processing: true,
              serverSide: true,
              ajax: "{{route('datatable.proyectoTribunales',$id)}}",
              columns: [
                  { data: 'id_prof', name: 'id_prof' },
                  { data: 'nombre', render: function ( data, type, row ) {
                    return row.nom_prof +' '+ row.app_prof + ' '+row.apm_prof;
                  }},
                  { data: 'proyectos', name: 'proyectos'},
                  { data: 'action', name: 'action', orderable: false, searchable: false}
              ],
              language: esp,
              searching: false, 
              paging: false,
              info:false,
          });
        });
    </script>
@endsection