@extends('master')
@section('title', 'Perfil')
@section('content')
@extends('cabecera-con-sesion-usuario') 

  <div class="container">
    <h3>Consejero</h3>
    <hr>
  <div class="row">
      <!-- left column -->
      <div class="col-md-5">
        <div class="text-center">
          <img src="{{ asset('uploads/avatars/'.Auth::user()->avatar) }}" class="avatar img-circle fotoPerfil" alt="avatar" style="border-radius: 50%;">
          <h6>Actualice su foto...</h6>
          <form class="needs-validation form-horizontal" enctype="multipart/form-data" action="{{ route('consejero.update.foto')}}" method="POST">
              <input type="file" name="avatar" style="width: 400px;" class="form-control">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">
              <br>
              <input type="submit" class="pull-center btn btn-primary btnColor" value="Cambiar Foto">
          </form>
        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-7 personal-info">
        <h4>Información Personal</h4>
        
        <form class="needs-validation form-horizontal" novalidate action="{{ route('consejero.perfil.update')}}" method="POST" enctype="multipart/form-data" >
          <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Carnet de Identidad:</label></div>
              <div class="col-md-6"><input name="ci_user" id="ci_user" class="form-control" pattern="^[A-Z]*?{1,3}\d{6,10}$" type="text" value="{{ $user->ci_user }}" required>
              <div class="invalid-feedback">
                  Introduzca entre números y/o letras.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Nombre:</label></div>
              <div class="col-md-6"><input name="nom_user" id="nom_user" class="form-control" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->nom_user }}" required>
              <div class="invalid-feedback">
                  Introduzca letras.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Paterno:</label></div>
              <div class="col-md-6"><input name="app_user" id="app_user" class="form-control" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->app_user }}">
              <div class="invalid-feedback">
                  Introduzca letras.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Materno:</label></div>
              <div class="col-md-6"><input name="apm_user" id="apm_user" class="form-control" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->apm_user }}" required>
              <div class="invalid-feedback">
                  Introduzca letras.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Teléfono:</label></div>
              <div class="col-md-6"><input name="tel_user" id="tel_user" class="form-control" pattern="^([0-9]+){7,20}$" type="text" value="{{ $user->tel_user }}">
              <div class="invalid-feedback">
                  Introduzca un número de teléfono válido
              </div>
            </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Dirección:</label></div>
              <div class="col-md-6"><input name="dir_user" id="dir_user" class="form-control" pattern="^[a-z ñ.áéíóúäëïöü\'-]+(\s*[a-z ñ.áéíóúäëïöü\'-]*)*[a-z ñ.áéíóúäëïöü\'-]+$" type="text" value="{{ $user->dir_user }}" required>
              <div class="invalid-feedback">
                  Introduzca entre números y/o letras.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Correo Electrónico:</label></div>
              <div class="col-md-6"><input name="email" id="email" class="form-control" type="text"  pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="{{ $user->email }}" required>
              <div class="invalid-feedback">
                  Introduzca un correo válido.
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-6 control-label"></label>
            <div class="col-md-8">
              
              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              <input type="submit" class="btn btn-primary btnColor" value="Guardar Cambios">
            </div>
          </div>
        </form>
      </div>
  </div>
</div>

@endsection