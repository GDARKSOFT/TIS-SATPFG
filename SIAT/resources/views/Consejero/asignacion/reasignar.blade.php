@extends('master')
@section('title', 'Reasignación de Tribunales')
@section('content')
@extends('cabecera-con-sesion-usuario')

<div class="container minHeight">
    <h2  class="title"><P>REASIGNAR TRIBUNAL</P></h2>
    @include('flash::message')
	<div class="containerTable">
    {!! Form::open(['route' => ['asignaciones.notificarReasignacion', $id, $idProfesional],'method' => 'post']) !!}
        {{  csrf_field() }}
	<table id="tablaAsignar" class="table table-hover table-table-bordered">
        <thead class="headTableColor">
            <tr class="titleSinNegrilla">
                <th></th>
                <th scope="col">Id</th>
                <th scope="col">Nombre</th>
                <th scope="col">N� Proyectos</th>
            </tr>
        </thead>
	</table>
        <button type="submit" class="btn btn-primary changeColorBtn btnColor">Reasignar</button>	
    {{ Form::close() }}
</div>
@endsection
@section('scripts')
    <script type="text/javascript">
          $(function() {
          $('#tablaAsignar').DataTable({
              processing: true,
              serverSide: true,
               ajax: "{{route('datatable.tribunales', $id)}}",
              columns: [
                  { data: 'check', name: 'check', orderable: false, searchable: false},
                  { data: 'id_prof', name: 'id_prof' },
                  { data: 'nombre', render: function ( data, type, row ) {
                    return row.nom_prof +' '+ row.app_prof + ' '+row.apm_prof;
                  }},
                  { data: 'proyectos', name: 'proyectos'}
              ],
              language: esp,
              searching: false, 
              paging: false,
              info:false,
          });
        });
    </script>
@endsection