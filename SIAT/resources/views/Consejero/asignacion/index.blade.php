@extends('master')
@section('title', 'Asignación de Tribunales')
@section('content')
@extends('cabecera-con-sesion-usuario')

<h2  class="title"><P>PROYECTOS HABILITADOS</P></h2>
	<div class="container">
		@include('flash::message')
		<div class="containerTable">
		<table id="tablaProyectosHabilitados" class="table table-hover table-table-bordered">
            <thead class="headTableColor">
                <tr class="titleSinNegrilla">
                    <th scope="col">Id</th>
                    <th scope="col">Carrera</th>
                    <th scope="col">Título Proyecto</th>
                    <th scope="col">Nombre Estudiante</th>
                    <th scope="col">Modalidad</th>
                    <th scope="col">Acciones</th>
                </tr>
            </thead>
		</table>
	</div>
    <br>
	<div class="btnFormat">
			<div class="row" style="text-align: center">
				<div class="col-md-2"><button type="button" class="btn btn-primary changeColorBtn btnColor" onclick="history.back()">Atrás</button></div>
			</div>
		</div>			
	</div>

@endsection
@section('scripts')
	<script type="text/javascript">
		$(function() {
	  $('#tablaProyectosHabilitados').DataTable({
	      processing: true,
	      serverSide: true,
	      ajax: "{{route('datatable.proyectosAsignacion')}}",
	      columns: [
	          { data: 'id', name: 'id' },
	          { data: 'carrera.des_carr', name: 'carrera.des_carr' },
	          { data: 'tit_proy', name: 'tit_proy' },
	          {
		          data: 'nom_est', render: function (data, type, row) {
		            return row.nom_est + ' ' + row.app_est + ' ' + row.apm_est;
		          }
		      },
	          { data: 'modalidad.des_mod', name: 'modalidad.des_mod'},
	          { data: 'action', name: 'action', orderable: false, searchable: false}
	      ],
	      language: esp
	  });
	});
	</script>
@endsection