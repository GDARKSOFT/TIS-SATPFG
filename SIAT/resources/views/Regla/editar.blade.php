@extends('master')
@section('title', 'Editar Regla')
@section('content')
@extends('cabecera-con-sesion-usuario')
<br>
        
        <br>
            <div class="card miCard marginDivUp">
                <h3 class = "titCard" align="center">Editar Regla</h3>
                <div class="card-body">
                     <h3 class = "titCard" align="left"></h3>

                        <form class="needs-validation" novalidate action="{{ route('reglas.update',$regla->id_reg)}}" method="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
                                                
                            <div class="form-group estiloCard">
                                
                                <input class="form-control" placeholder="Descripcion"  name="des_reg" type="text"  id="des_reg"  value="{{ $regla->des_reg }}" maxlength="100" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" title="Introduzca una descripcion entre 5 y 100 letras" minlength="5" required>
                                <div class="invalid-feedback">
                                    Introduzca una descripción entre 5 y 100 letras
                                </div>
                            </div>
                            <div class="form-row align-items-center estiloCard">
                                <div class="col-md-3 my-1">
                                    <label for="label" class = "card-text  mr-sm-2"> Valor </label>
                                </div>
                                <div class="col-md-9 my-1">
                                    <input id="val_reg" name="val_reg" type="number" class="form-control"  value = "{{ $regla->val_reg }}" maxlength="100"  title="Introduzca un numero" minlength="5" required>
                                    <div class="invalid-feedback">
                                        Introduzca un valor numérico
                                    </div>
                                </div>   
                            </div>

                            <div class="estiloCard">
                                
                                <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                                <button type="submit" class="btn btnColor btnCard">Guardar Cambios</button>

                            </div>
                        </form>
                     </div>
                    
                </div>
            

@endsection
           