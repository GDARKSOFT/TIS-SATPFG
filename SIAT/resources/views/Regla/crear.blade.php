@extends('master')
@section('title', 'Nueva Regla')
@section('content')
@extends('cabecera-con-sesion-usuario')  
            <div class="card miCard marginDivUp">
                <div class="card-body">
                     <h3 class = "titCard" align="center">Nueva Regla</h3>
                     @include('flash::message')
                     <div>
                        <form class="needs-validation" novalidate action="{{ route('reglas.store') }}" method ="POST" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                            
                            <div class="form-group estiloCard">
                                <input name="des_reg" type="text" class="form-control" placeholder="Descripción" value = "{{old('des_reg')}}" maxlength="100" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" title="Introduzca una descripcion entre 5 y 100 letras" minlength="5" required>

                                <div class="invalid-feedback">
                                    Introduzca una descripción entre 5 y 100 letras
                                </div>
                            </div>
                            <div class="form-row align-items-center estiloCard">
                                <div class="col-md-3 my-1">
                                    <label for="label" class = "card-text  mr-sm-2"> Valor </label>
                                </div>
                                <div class="col-md-9 my-1">
                                    <input name="val_reg" type="number" class="form-control" value = "{{old('val_reg')}}" maxlength="100" title="Introduzca un numéro" minlength="5" required>
                                    <div class="invalid-feedback">
                                        Introduzca un valor numérico
                                    </div>
                                </div>   
                            </div>
                            <div class="estiloCard">
                                <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
              
                                <button id="guardar" class="btn btnColor btnCard" type="submit" data-toggle="modal" data-target="#mensaje" value ="1">Guardar</button>

                                 
                            </div>
                        </form>
                    </div>
                </div> 
            </div>
            <br>
@endsection
           