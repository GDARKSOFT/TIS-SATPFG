@extends('master')
@section('title', 'Reglas')
@section('content')

@extends('cabecera-con-sesion-usuario')

	

<!-- Image and text -->
<h2  class="title">REGLAS</h2>
	<div class="container">
		@include('flash::message')
		<div class="containerTable">
		<table id="tablaRegla" class="table table-bordered table-hover">
		  <thead class="headTableColor">
				<tr class="titleSinNegrilla">
					<th scope="col" >N°</th>
					<th scope="col" style="width: 500px;">Descripción</th>
					<th scope="col">Valor</th>
					<th>Acción</th>
				</tr>
		  </thead>
		</table>
		</div>
		<br>
		<div class="btnFormat">
			<div class="row" style="text-align: center">
				<div class="col-md-2"><button type="button" onclick="history.back(-1)" " class="btn btn-primary changeColorBtn btnColor">Atrás</button></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"><a href="{{route('reglas.create')}}" class="btn btn-primary changeColorBtn btnColor">nueva regla</a></div>
			</div>
		</div>		
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		  $(document).ready(function() {
		  $('#tablaRegla').DataTable({
		      processing: true,
		      serverSide: true,
		     ajax: "/TIS-SATPFG/SIAT/public/reglas/datos",
		     ajax: "{{route('datatable.reglas')}}",
		      columns: [
		          { data: 'id_reg', name: 'id_reg' },
		          { data: 'des_reg', name: 'des_reg', width: '60%' },
		          { data: 'val_reg', name: 'val_reg' },
		          { data: 'action', name: 'action', orderable: false, searchable: false}
		      ],
		      language: esp
		  });
		});
	</script>
@endsection