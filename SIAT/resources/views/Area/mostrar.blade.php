@extends('master')
@section('title', 'Ver Área')
@section('content')
@extends('cabecera-con-sesion-usuario')
<h4  class="title ">Área: {{ $area->nom_area }}</h4>
<div class="container"></div>
<div class="containerLados">
		<h6>{{$area->des_area}}</h6>
		<br>
    <table class="table table-bordered table-hover">
		  <thead class="headTableColor">
			<tr class="titleSinNegrilla">
			  <th scope="col" >N°</th>
			  <th scope="col">Subárea</th>
			</tr>
		  </thead>
		  <tbody>
			
			@foreach ($subareas as $subarea)
			<tr>
			  <td scope="row">{{ $subarea->id}}</td>
			  <td><label>{{ $subarea->nom_area}}</label></td>
			</tr>
		
			@endforeach
		  </tbody>
		</table>
		</div>
		<div class="btnFormat">
			<div class="row" style="text-align: center">
					<div class="col-md-3"></div>
					<div class="col-md-1"><button type="button" onclick="history.back(-1)" class="btn btn-primary changeColorBtn btnColor">Atrás</button></div>
					<div class="col-md-8"></div>
			</div>
		</div>
</div>
@endsection