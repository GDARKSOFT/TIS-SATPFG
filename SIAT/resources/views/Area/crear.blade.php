@extends('master')
@section('title', 'Nueva Área')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="card miCard marginDivUp marginDivBotton">
      <div class="card-body ">
           <h3 class = "titCard" align="center">Nueva Área de Conocimiento</h3>
           @include('flash::message')
           <div>
              <form class="needs-validation" novalidate action="{{ route('areas.store') }}" method ="POST" enctype="multipart/form-data">
                    <input  type="hidden" name="_token" value="{{ csrf_token() }}"/>
                  <div class="form-group estiloCard">
                    
                    <input  id="nombre" name="nom_area" type="text" class="form-control" placeholder="Área de conocimiento" value = "{{old('nom_area')}}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" minlength="5" required>
                    <div class="invalid-feedback">
                      Introduzca una descripción entre 5 y 100 letras
                    </div>
                  </div>

                  <div class="form-group estiloCard">
                    <textarea rows="4" name="des_area" class="form-control" placeholder="Descripcion del área de conocimiento" value = "{{old('des_area')}}"></textarea>
                  </div>
          
                  <div class="estiloCard">
                      <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
                      <button id="guardar" class="btn btnColor btnCard" type="submit" data-toggle="modal" data-target="#mensaje" value ="1">Guardar</button>

                  </div>
              </form>
           </div>
      </div>
  </div>
  <br>
  <br>
@endsection