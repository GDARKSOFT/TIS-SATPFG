@extends('master')
@section('title', 'Editar Área')
@section('content')
@extends('cabecera-con-sesion-usuario')

@if(count($errors) > 0)
    <div class="alert alert-danger" role="alert">
        <ul>
            @foreach($errors -> all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
    
    <br>
            <div class="card miCard marginDivUp">
                <h3 class = "titCard" align="center">Editar Área</h3>
                <div class="card-body">
                     <h3 class = "titCard" align="left"></h3>
                        @include('flash::message')
                        <form class="needs-validation" novalidate action="{{ route('areas.update', $area->id)}}" method="POST">
                                    <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}

                            <div class="form-group estiloCard">
                                
                                <input class="form-control" name="nom_area" type="text" id="nom_area" placeholder="Nombre del area" value="{{ $area->nom_area }}" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" title="Introduzca una descripcion entre 5 y 100 letras" minlength="5" required>
                                <div class="invalid-feedback">
                                    Introduzca una descripción entre 5 y 100 letras
                                </div>
                            </div>
                            <div class="form-group estiloCard">
                                <textarea rows="4" name="des_area" class="form-control" placeholder="Descripcion del área de conocimiento">{{ $area->des_area }}</textarea>
                            </div>
                            @include('flash::message')
                            <div class="">
                            <table class="table table-bordered table-hover">
                                <thead class="headTableColor">
                                    <tr class="titleSinNegrilla">
                                    <th scope="col" >N°</th>
                                    <th scope="col"> Subárea</th>
                                    <th colspan="2">Acción</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($subareas as $subarea)
                                    <tr>
                                    <td scope="row">{{ $subarea->id}}</td>
                                    <td>{{ $subarea->nom_area}}</td>
                                    <td><a href='{{ route('subareas.edit', ['id' => $area->id, 'idSub' => $subarea->id]) }}' class="colorIcon"><i class="fas fa-pencil-alt fa-lg"></i></a></td>
                                    <td><a class="colorIcon delete-modal"  data-toggle="modal" data-target="#modal-delete"
                                    data-delete-link="{{ route('subareas.destroy',['id' => $area->id, 'idSub' => $subarea->id]) }}"
                                    data-body="Esta seguro de eliminar el subarea: {{ $subarea ->id }}"><i class="fas fa-trash-alt fa-lg"></i></a>
                                    </td>                                       
                                    </tr>
                                    @endforeach
                                </tbody>
                                </table>
                                </div>

                            <div class="estiloCard">
                                
                                <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
                                <a href="{{route('subareas.create', $area->id)}}" class="btn btn-primary changeColorBtn btnColor">Agregar subárea</a>

                                <button type="submit" class="btn btnColor btnCardA">Guardar Cambios</button>
                            </div>
                        </form>
                     </div>                   
                </div>
@endsection
