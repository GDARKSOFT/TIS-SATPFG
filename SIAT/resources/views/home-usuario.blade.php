@extends('master')
@section('title', 'Home')
@section('content')
	@extends('cabecera-con-sesion-usuario')
	<div class="container">
	@include('flash::message')
		<div class="jumbotron content">
			<h1 class="display-4">¡Bienvenido al SIAT!</h1>
			<p class="lead">Sistema de Información para la Asignación de Tribunales</p>
			<hr class="my-4">
		</div>
	</div>
@endsection