@extends('master')
@section('title', 'Perfil')
@section('content')
@extends('cabecera-con-sesion-usuario') 

  <div class="container">
    <h3>Secretaria</h3>
  	<hr>
	<div class="row"> 
      <!-- left column -->
      <div class="col-md-5">
        <div class="text-center">
          <img src="{{ asset('uploads/avatars/'.Auth::user()->avatar) }}" class="avatar img-circle fotoPerfil" alt="avatar" style="border-radius: 50%;">
          <h6>Actualice su foto...</h6>
          <form enctype="multipart/form-data" action="{{ route('secretaria.update.foto')}}" method="POST">
            <input type="file" name="avatar" style="width: 400px;" class="form-control">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <br>
            <input type="submit" class="pull-center btn btn-primary btnColor" value="Cambiar Foto">
          </form>
        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-7 personal-info">
        <h4>Información Personal</h4>
        
        <form class="needs-validation form-horizontal" novalidate action="{{ route('secretaria.perfil.update')}}" method="POST" enctype="multipart/form-data" >
          <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Carnet de Identidad:</label></div>
              <div class="col-md-6"><input name="ci_user" id="ci_user" pattern="^[A-Z]*?{1,3}\d{6,10}$" class="form-control" type="text" value="{{ $user->ci_user }}" required>
              <div class="invalid-feedback">
                  IIntroduzca el <i>Carnet de Identidad</i> entre 6 y 15 caracteres
              </div>
              </div>
              
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Nombre:</label></div>
              <div class="col-md-6"><input name="nom_user" id="nom_user" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->nom_user }}" required>
                <div class="invalid-feedback">
                  Introduzca el <i>Nombre</i> entre 2 y 100 letras.
              </div>
              </div>
              
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Paterno:</label></div>
              <div class="col-md-6"><input name="app_user" id="app_user" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->app_user }}">
                <div class="invalid-feedback">
                  Introduzca el <i>Apellido Paterno</i> entre 2 y 100 letras
              </div>
              </div>
              
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Apellido Materno:</label></div>
              <div class="col-md-6"><input name="apm_user" id="apm_user" pattern="^([A-ZÁÉÍÓÚa-zñáéíóúäëïöü]+[\s]*)+$" class="form-control" type="text" value="{{ $user->apm_user }}" required>
                <div class="invalid-feedback">
                    Introduzca el <i>Apellido Materno</i> entre 2 y 100 letras
                </div>
              </div>
              
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Teléfono:</label></div>
              <div class="col-md-6"><input name="tel_user" id="tel_user" pattern="^([0-9]+){7,20}$" class="form-control" type="text" value="{{ $user->tel_user }}" required>
                <div class="invalid-feedback">
                  Introduzca el <i>Telefono</i> entre 7 y 20 numeros
              </div>
            </div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Dirección:</label></div>
              <div class="col-md-6"><input name="dir_user" id="dir_user" pattern="^[a-z ñ.áéíóúäëïöü\'-]+(\s*[a-z ñ.áéíóúäëïöü\'-]*)*[a-z ñ.áéíóúäëïöü\'-]+$" class="form-control" type="text" value="{{ $user->dir_user }}">
                <div class="invalid-feedback">
                  Introduzca la <i>Direccion</i> entre 10 y 100 caracteres
              </div>
            </div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-4"><label class="control-label">Correo Electrónico:</label></div>
              <div class="col-md-6"><input name="email" id="email" class="form-control" type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" value="{{ $user->email }}" required>
              <div class="invalid-feedback">
                  Introduzca una dirección de <i>Correo Electronico</i> válida
              </div>
              </div>
              
            </div>
          </div>

          <div class="form-group">
            <label class="col-md-6 control-label"></label>
            <div class="col-md-8">
              <input type="submit" class="btn btn-primary btnColor" value="Guardar Cambios">
              <span></span>
              <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
            </div>
          </div>
          
        </form>
      </div>
  </div>
</div>

@endsection