@extends('master')
@section('title', 'Mostrar Proyecto')
@section('content')
@extends('cabecera-con-sesion-usuario') 
  <div class="container">
    <h3>Proyecto</h3>
    <hr>
    <div class="row"> 
      <!-- left column -->
      <div class="col-md-3">
        
      </div>
      <!-- edit form column -->
      <div class="col-md-8 personal-info">
        <h4>Información del Proyecto</h4>
        
        <form  action="" method ="POST" enctype="multipart/form-data">
          <input type="hidden" name="_method" value="PUT">
                                    {{ csrf_field() }}
          <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Proyecto ID:</div>
              <div class="col-md-6">{{ $proyecto->id }}</div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Título Proyecto:</div>
              <div class="col-md-6">{{$proyecto->tit_proy}}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Nombre:</div>
              <div class="col-md-6">{{$proyecto->nom_est}}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Apellido Paterno:</div>
              <div class="col-md-6">{{$proyecto->app_est}}</div>
            </div>
          </div>
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Apellido Materno:</div>
              <div class="col-md-6">{{$proyecto->apm_est}}</div>
            </div>
          </div>
          
          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Nombre Tutor:</div>
              <div class="col-md-6">{{$datosTutor->nom_prof}}{{$datosTutor->app_prof}}{{$datosTutor->apm_prof}}</div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Área:</div>
              <div class="col-md-6">{{$datosArea->nom_area}}</div>
            </div>
          </div>

          <div class="form-group">
            <div class="row" >
              <div class="col-md-3">Modalidad:</div>
              <div class="col-md-6">{{$datosProyModa->des_mod}}</div>
            </div>
          </div>

        </form>
        @if(Auth::user()->roles()->first()->id == 2)
        @include('flash::message')
                        
                        {!!Form::open(array('route' =>['proyectos.update',$proyecto->id], 'files'=>true, 'method'=>'put'))!!}
                            {{  csrf_field() }}
                            <div class="row" >
                                <div class="col-md-2 "></div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        @if($proyReq[0]->entregado == 0)
                                            <input id="opA" type="checkbox"  name="opA" class="form-check-input" value="a" checked="true" >
                                            <label class="form-check-label">Carta del estudiante al H.C.C. solicitando la designación de tribunales.</label><br>
                                        @else
                                            <input id="opA" type="checkbox"  name="opA" class="form-check-input" value="a" >
                                            <label class="form-check-label">Carta del estudiante al H.C.C. solicitando la designación de tribunales.</label><br>
                                        @endif
                                        @if($proyReq[1]->entregado == 0)
                                            <input id="opB" type="checkbox"  name="opB" class="form-check-input" value="b" checked="true">
                                            <label class="form-check-label">Carta del tutor al H.C.C. indicando que el trabajo de grado esta concluido.</label>
                                        @else
                                            <input id="opB" type="checkbox"  name="opB" class="form-check-input" value="b" >
                                            <label class="form-check-label">Carta del tutor al H.C.C. indicando que el trabajo de grado esta concluido.</label>
                                        @endif
                                        @if($proyReq[2]->entregado == 0)
                                            <input id="opC" type="checkbox"  name="opC" class="form-check-input" value="c" checked="true">
                                            <label class="form-check-label">Carta del docente de materia de modalidad de titulación al H.C.C. indicando que el trabajo de grado es apto para la revisión.</label><br>
                                        @else
                                            <input id="opC" type="checkbox"  name="opC" class="form-check-input" value="c" >
                                            <label class="form-check-label">Carta del docente de materia de modalidad de titulación al H.C.C. indicando que el trabajo de grado es apto para la revisión.</label><br>
                                        @endif
                                        @if($proyReq[3]->entregado == 0)
                                            <input id="opD" type="checkbox"  name="opD" class="form-check-input" value="d" checked="true">
                                            <label class="form-check-label">Tres copias del documento del trabajo de grado (anillados).</label>
                                        @else
                                            <input id="opD" type="checkbox"  name="opD" class="form-check-input" value="d" >
                                            <label class="form-check-label">Tres copias del documento del trabajo de grado (anillados).</label>
                                        @endif

                                        @if($datosProyModa->des_mod == 'Adscripción' || $datosProyModa->des_mod == 'Trabajo Dirigido')
                                            @if($proyReq[4]->entregado == 0)
                                                <input id="opE" type="checkbox" name="opE" class="form-check-input" checked="true">
                                                <label class="form-check-label">Carta del responsable de la unidad, indicando que el trabajo de grado ha sido concluido satisfactoriamente.</label>
                                            @else
                                                <input id="opE" type="checkbox" name="opE" class="form-check-input">
                                                <label class="form-check-label">Carta del responsable de la unidad, indicando que el trabajo de grado ha sido concluido satisfactoriamente.</label>
                                            @endif
                                        @endif
                                    </div>
                                    <div class="container">
                                        <div class="row" >
                                            <div class="col-md-2 "></div>
                                            <div class="col-md-4">
                                            <button class="btn btn-default" type="reset" onclick="history.back()">Cancelar</button>
                                            </div>
                                            <div class="col-md-1 "></div>
                                            <div class="col-md-4 alineadoRight"><button type="submit"class="pull-center btn btn-primary changeColorBtn btnColor" align="center">Guardar Cambios</button></div>
                                            <div class="col-md-1 "></div>
                                        </div>
                                    </div><br>
                                </div>
                                <div class="col-md-2 "></div>
                            </div>
                            {{ Form::close() }}
          @endif
      </div>
      
  </div>
</div>

@endsection

