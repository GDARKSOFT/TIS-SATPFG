@extends('master')
@section('title', 'Proyectos')
@section('content')
@extends('cabecera-con-sesion-usuario')

<!-- Image and text -->
<h2  class="title">PROYECTOS</h2>
	<div class="container">
		@include('flash::message')
		<div class = "containerTable">
			<table id="tablaProyecto" class="table table-hover table-bordered">
				<thead class="headTableColor">
				<tr>
					<th scope="col">Id</th>
                    <th scope="col">Carrera</th>
					<th scope="col">Titulo Proyecto</th>
					<th scope="col">Estudiante</th>
                    <th scope="col">Modalidad</th>
					<th scope="col">Estado</th>
                    <th scope="col">Acciones</th>
					
				</tr>
				</thead>
			</table>
		</div>
		<br>
		<form class="form-import needs-validation" novalidate method="post" id="frm" action="{{ url('/proyectos/importar') }}" files="true" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input class="selectionFile btn btnFile tamLeft form-control" type="file" name="excel" required>
				<button class="btn btnColor tamRight" type="submit"> Importar  <i class="fas fa-download fa-lg"></i></button>
				<div class="invalid-feedback tamLeft">
					Archivo .csv requerido para importar
				</div>
		</form>
		<div class="btnFormat">
			<div class="row" style="text-align: center">
				<div class="col-md-2"><button type="button" onclick="history.back(-1)" class="btn btn-primary changeColorBtn btnColor">Atrás</button></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-2"></div>
				<div class="col-md-1"></div>
			
			</div>
		</div>	

	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		  $(function() {
		  $('#tablaProyecto').DataTable({
		  	  processing: true,
		      serverSide: true,
		      "dom": '<Bf<lt>ip>',
		        buttons: [
		            {
		            	extend: 'copy',
		            	text:'<i title="Copiar" class="far fa-copy"></i>',
		            	className: 'btnArriba',
		            	exportOptions: {
                    		columns: [ 0, 1, 2, 3, 4]
                		},
		            },
		            {
		                extend: 'excel',
		                title: 'ProyectosExcel',
		                text: '<i title="excel" class="far fa-file-excel"></i>',
		                className: 'btnArriba',
		                exportOptions: {
                    		columns: [ 0, 1, 2, 3, 4]
                		},
		            },		           		            
		          	{
		          		extend: 'csv',
		          		title: 'ProyectosCSV',
		          		text: '<i title="csv" class="fas fa-file-excel"></i>',
		          		className: 'btnArriba',
		          		exportOptions: {
                    		columns: [ 0, 1, 2, 3, 4]
                		},
		          	},
		          	{
		                extend: 'pdfHtml5',
		                customize: function ( doc ) {
	                    doc.content.splice( 1, 0, {
	                        margin: [ 0, -30, 0, 10 ],
	                        alignment: 'right',
	                        image: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAArCAYAAACO7C3tAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAA7sSURBVHhe7VoJcJRFFv7mykwuCIQjJARCEo7IJYWIeCGCoEaBCOtaW6Uulq7HWipYq5ZbtdaWbnnUVq1VorUeW4Xuulul3CCIkBJBRPBgJcKCIkcMEHKRZDLJ3LPvdf890xn+GSaEK5Rf6s//5nv9+u/u1+919z9jCYfDEYvFgkgkAoaS+c4gPaxWq5AVdE6VTcVe8RejvS6fqb3iu2NvIUL25BdcFLAEg8GIzWYTnhIEeSsUCoE5RiRCXrNY4Q0E8cK/t6HuRCsaPD7YrBbysihyXuFp92PB7Cswb8pwg5FI1H59NirejGN7vpg/U3vmWdbtU9UrOaUIqXhhBVZ+sB3IywHsZCxC7EIEFj23tQN/WXgznr1jssFdWrAqf/BdlxVufW4ZVm78HhieT86wA/4QKFzoCl6Ai58bgD+Y+mTQ+2LWP51Lpmck0/M9mZ6RTM93viwUJhEOPw4nhh6ydW4vBl73Z3JGHtDYhrtmjsOc60aisG860miyBkNUCd3lkhQDP0JxuqzQHb0/EMKIwbkYlNvLYJJDTwkq1cSnCZbVgCTS67LOqTrZVqUkxTFoSaB5TBOZoHgzTtknTVkL36zEq6u+JS9ZMO/KEiz9U4Xgl+734p39HmTYeaj4iq/CjEuErtm3ByK4b2wm7ix2GcylhaSL+lWL3seOgyeAlg5sWnwvpo8eLMrsaQ7gp9YQHDQJaJNIfzyADDmQOqcPrUXTxLiu2FPGoow5LseG4t4OSXYBql/6DFYc958v5s0igHE6e+ZZ1u1T1Ss5aYTk3fsWTtAiirpWHPvoSQzKyRD8sn0evPdjhxEh5xftwTDuGZ2FecXpBnNpIalDCu/7B2qaPUC9G4dXPoahudmCr2oI4JsmP9Js598hPlrQrxroRFmOzMGXGpIu6rOfW4E131XzKODVR6bj8dsvF2U2HPHgi7oQnOSQYJDK2mlREuaUaGi9CdEstvP2mBAUdUk9+95KNqEQ6Y0wjdrTtBD6BPact8LhCHy0iygf4sCUvNQixCw9mHH87FRSSrxepS/dXnEMs7JmHIN5kuUHvitZFZ57DR2+2nxAlgtPLK7Ezw1uweekZ1Aup8MiXSFrGu2EbQhEbAjCgUCYGmQjju58RSxOBMN2hGBHmMoGIzSzrcRxebpgdyJEXJhsQfpwxAGL3SXK82W1ucgXDqqHeSc93Q5neurpSvWFoWQzjtfOZHqGmV6NmW6vOIZZWTOOwfxp32VZZv4V6EUDwOmpzo2bppRgREl/XF6YAysts0HyME1cuniG0GXMZDFjxEWep/Dg6kPMcxkaYjITdwpQahQQoKgQtno9ZKfKCTuj7vZAmI4lcjYO6O3CqPw+KL+8EAX9s+E0IktB74uSE3GMRHpdPqd6ChORsvgDg5V6GLXQWSRn7mtAOs3gjDTKITRCdBYQd7YRdckKo4j7KIhTOEInzvjQFVtuMnuJ2+OhSM7NRAVtz99dOB3ZGXJbrHdY9SsRx2A+QpzF6H+q9gzFm3GMlOzpn2xJEvgp589/eT3W7TyEkNdPDFXGEWNUek7Bj+AWdnqUQfLzuTO8/zbWGfjoNH+yHR++8ivMn1wsSncFgUBADJCeVs4nki7qDF1mbK2qQW19K440d6DDHxR2NisvipxYyN5C9mGyIY6hZD5ZsO+T6mm2W2lgA3SStVpoLSIuECCZNwVUeYA2APyik+/M+SlKjzS6setgI36ub6OZEwSyKTK4IdWNOLzqUQwdIHeGDNUXs/7trtqLEyca4PP5xYYiMyMdRUVDUFiYL8oxNlVuRVqaHYWDCzBs2BDB6XXt+u/3aGvzIEhtnjbtGsEp/aebv0Cag9ZPNcEU6DOPYZ8+ORgzelRqEcLwUa6v9UYoa9HsMSakmLTqAeIDQcmqVjM5Fb0BTYxC51jOpOtkux+L3t6CJRv20K6DzkvktJI+mTjw5t2iXDJs3folTja30mS0wO5wIEyDyBOBo2XEiFKMGztKlFu+Yh05xIHhw4sxckSJ4HQsXbYWWVmZ8Hp9mHzlBAwaNNDQAB98uBppTqdoL9fB0R2mzMOTj+95A/vh6qsnxRZ1HewjxSl5TbUXs5fVoXeGnK3RUVGDGY94PX/Wy54Fe25HG02U8O8LDRK48g8f4qtDTdRrmrWHGhDZ/oyh4fKyL/H944HMyMxEMc36slGl6PB6KVrqsXv3/zD79pmiHGPFyvVwOtNQWjoMI8gpDFXXnj37caS6Rsgy5VkxY/r1UX1j40lxJHCSw7fv+BZ+vx/5g/JQXDxUvNtyUPRkZ2eZL+p6GOqLTk/Auu9qUP7McoCiA01tWPtiBconyPSi+qL3ye8PYM3aT2ig6bBZNhwlNECJwBHicjk7OUTVtZyclZHuQu+cXmiiwefUN2vmVGSSo2PgMbaI1Ofz+VA0tBCjR4+UKgLXlXLK2t0YwEvfuNErjWaYwV1wUEN81PwlN/Y1CKDB7UP/OW8A+b3FO7g3npiOh6fLlJMIq1ZvEGmED6lZWRno2zcHgwvykZvbxyghsXLVx6Kc7hDG4SM/o6pqH3w06+ffUY4Nn2wWg9urdzaumTLJKBWDcsjQIYMxZkzntpl+H8KVKSjZ5bCgOMuKIVk2FGVaMZTufBURJ++6rOnNyp4t+2wbhmTKSI62Xdw5HclLn29m/WPcPOsGMdBh2mC43R5UVx/D1s93YOOmz4wSEtG6tDoZe/f+KDYCRTTAjLFjy0SWqT1eL+4K8c/VwXWz3vRgyAoV0krmQ12jn87TlAdFg6icgC4bIXm+9PyJD4r9XPLVB7d9875aTFu0VJxJ0OjBxy9VYNa4AmGt+mLWP4bb3YZaWjuOHasVjuFFPjMzA1OvnyL00TWkpIgWe7moNzY2kfN2Cp4X8XRKWzaqb9/+A6LegoJBGD/uMlFWPcssQrj9og/0j/t1Wmw46sOvVzegTzoPhDEoDB6s+CoMLqWKuwF+bBs5pOl+OeCMx97ehtc+3isPsYebENqykL/OSYqamuMYMKCf3P0Y+G73XuEY3jFVzL1FcGYp67Mt29HR4RWDLXZM1B5+HC/SDlrAeRt8R8WtoqxCspR12h859CSs31WDW/+4WkYHDYyDLv9/Fhja2JlAj4qWllYxQM40WtQvK6Xt5wDx4nPHV7vgae9AiNaV28pniLLxizo7a936SnJSmjivsBPEMNI/O8k//HBQjOdltHMrpqhSz486ZCg5hM4eCtyupBHyzYF6LK7cj81VxxAO0N6ctpg8+W38j6BmHlfAIn8BxTDU4i44KiA48TkGaa8zcRAti2teNGXygNLdRbPaF6JDagDVx91AbzoY8sNOtOLtZ2bi/mmdf50Sj+aWFmzb9jUNFh0qubVGm3jb2k4zf8rkiZR28pg0ziFpdA4ZJs4hO8lp9fWNIp3Pvu0mUUbHlzt3obGBopQcoW+fN27aIpxZRA7h9UaH6RoSInnCohWo+qFOdthpnDBFa40BihunCwbRbmoX+4dPq/yxuR0Ty/Lw9ctzOr2XUpGhR4jCgZ8O4+jRWnE+4DcGrnQnRo0sFalMYf2GT+m07aATfKHYHn+0rlKsHSzzyT2+fk97Ox06d5ClBZMmjUduX7lr+4wOokE6dPL6ws9QYLtODuE7w1LxjjxYsTMUuOOpOiFa7jQGKdeXQkEaRHQExIvGB28fg78/fK2ktcFXfTTjFFjH3OnOYfF2/JkRP5YKul6HXlbI9K9Tb8c8uRJ7jjYbUUEFuZOtXkoF6bCzkwgcogzOHlYqw7sRrpJTGVdutciH8Ot5UUboJa/Kcxe5DP/gjmhxihVlhb28SxvZDMUzp96wy/otYudnS3fgpvH5eGBqKXKy+HuTnonoSZ3xU0MbSu96DxicIz7zW9M7byjF6/dNhiONjvZpVvGNH7mSlGJ0pCzyuQKTvN8+dVZdCOgzUM32RByD+UQRlMyeofhu2RMRfZf19JKdeGX9PiCdoiMQxrXFudj6vNzyMbbV+PB5rR9OmqGy+TGfKAhXEak4JYsnRHneEguG/vP+m+c9Q/IWChnmBIRs2BNE/cZdoZOehNb2EJ6/TuZrvfNKTsQxEul1+Zzq6V+0b+OeXouqYy20iaYRb2pH5YvluLEs9sbyvf0erD7shYt/baKsuC59dBg61xW9krujJxz3hFA5u7/80MPQKWVZfvMvcgZFB6elmhZEPvkddZbSD6UkPbR6EtTeX5fNOJ6X3MdEel3WOT0NKXt9rPhN7hn/ctFS8S7oKE4COaS6CZGNDxoaiee2t+BvVW3IpAgxm6AK8ZM2Vb2Su6Nn1LYGEXlCvuHtaYie1MWH+f8EetEOhbcz1c0UIQ9Qb6m7IsdFUOMOobYjDDvndaP7Fv6mkCJIDYYYMINjsOu4rNQr+VR7IdN1Nuw7aP27Ol9+p67PViXrnB4hfDFvFgEMxXfFPlU9g+vttKj3fXgFTnb4ATs9rM6DvW/NQ1me/AqUK1PlehL0dis5EcdIpNflRBzjTPVRmf7JkoQFi7/Akh3V8gziD2HO+EFY+eT1hhZ4aGMj3tzRKn9UIOZiKuCHxZc14xKhi/asaqKU9VLsBNyT0GlRr9xXjxlPrQMGZsn+NnXgkblleP2eiULfM8EdYS/F0oNZyuB5yVeylGJmn2pKYyg+mf0pJ/Xs+5eLV9pip8Ud8VAKC4Xx2+klmFLUBy5aY3JcdiPMEszSswBulfo6Rw5nDLEh7izrCJEiQO0uHZCFiUPkQZe7qtJDPNQwJNKngmT1p4pTHOL2BtFr7vtAAa0dhufE6PiCwjFiBPhzJ6tzgO71S4L68mj5KLz2wBUGcfHD9PuQ424vCh5ag0gwRCFDuy5e5M+1A84FvAE8NaMUL98tfySeLKVw//k6XUpJZs88y7p9qnolnxIhOhZvOIBnV+2D+2CTdAy/M+EtMSPlGcwF4x9hxiVCN+w7gnj8lhF4dcEEg7j4EXUIe4/BntdngMKyXcfx7aGTCHsCcPsDNBxSz2X5TS1NAfGZQkz+AtGwD4aDEL9SjKmJC1HQxU6q0p6roBxMsqk9O8Eom9Ce/jjClb03EMKssn64c+qwaFnmk52UebYqjtHVk3Z37ZNGyC843wD+DwWXD9WllLfzAAAAAElFTkSuQmCC'
	                    } );
               		 },
		                title: 'ProyectosPDF',
		                text: '<i title="pdf" class="far fa-file-pdf" ></i>',
                		className: 'btnArriba',
		                exportOptions: {
                    		columns: [ 0, 1, 2, 3, 4]
                		}
		            },
		          	{
		                extend: 'print',
		                text: '<i title="Imprimir" class="fas fa-print" ></i>',
		                className: 'btnArriba',
		                title: 'Reporte Proyectos',
		                exportOptions: {
                    		columns: [ 0, 1, 2, 3, 4]
                		},
		                messageBottom: null
		          	},
		        ],
		      ajax: "{{route('datatable.proyectos')}}",
		      columns: [
		          { data: 'id', name: 'id' },
		          { data: 'carrera.des_carr', name: 'carrera.des_carr' },
		          { data: 'tit_proy', name: 'tit_proy' },
		        {
		          data: 'nom_est', render: function (data, type, row) {
		            return row.nom_est + ' ' + row.app_est + ' ' + row.apm_est;
		          }
		        },
		          { data: 'modalidad.des_mod', name: 'modalidad.des_mod'},
		          { data: 'estado.des_est', name: 'estado.des_est'},
				  { data: 'action', name: 'action', orderable: false, searchable: false }  
		      ],
		      language: esp 
		  });
		});
	</script>
@endsection