@extends('master')
@section('title', 'Error 404')
@section('content')
<div class="cuerpo centrar-items">
    <div>
        <img src="{{ asset('images/500.png') }}" width="" height="300" alt=""><br/>
        <h2 align="center">Error inesperado</h2>
        <br>
        <div class="centrar-items">
            <a href="{{ route('home') }}" class="btn btn-primary changeColorBtn btnColor">Página Principal</a>
        </div>
    </div>
</div>
@endsection