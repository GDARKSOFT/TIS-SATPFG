@extends('master')
@section('title', 'Error 403')
@section('content')
<div class="cuerpo centrar-items">
    <div>
        <img src="{{ asset('images/403.png') }}" width="" height="300" alt=""><br/>
        <h2 align="center">Acceso Denegado</h2>
        <br>
        <div class="centrar-items">
            <a href="{{ route('home') }}" class="btn btn-primary changeColorBtn btnColor">Página Principal</a>
        </div>
    </div>
</div>
@endsection