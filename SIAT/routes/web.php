<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/reglas/datos', 'DataTableController@getReglas')->name('datatable.reglas');
Route::get('/areas/datos', 'DataTableController@getAreas')->name('datatable.areas');
Route::get('/usuarios/datos', 'DataTableController@getUsuarios')->name('datatable.usuarios');
Route::get('/roles/datos', 'DataTableController@getRoles')->name('datatable.roles');
Route::get('/profesionales/datos', 'DataTableController@getProfesionales')->name('datatable.profesionales');
Route::get('/proyectos/datos', 'DataTableController@getProyectos')->name('datatable.proyectos');
Route::get('/proyectos/datosAsignacion', 'DataTableController@getProyectosAsignacion')->name('datatable.proyectosAsignacion');
Route::get('/proyectos/{id}/profesionales/datosAsignacion', 'DataTableController@getTribunales')->name('datatable.tribunales');
Route::get('/proyectos/datosAsignados', 'DataTableController@getProyectosAsignados')->name('datatable.proyectosAsignados');
Route::get('/proyectos/{id}/profesionales', 'DataTableController@getTribunalesAsignados')->name('datatable.proyectoTribunales');
Route::get('/profesionales/proyectosAsignados', 'DataTableController@getProyectosProfesional')->name('datatable.proyectosAsignadosProfesional');
Route::get('/profesionales/competencias', 'DataTableController@getCompetencias')->name('datatable.competencia');
Route::get('/profesionales/intereses', 'DataTableController@getIntereses')->name('datatable.interes');

Route::get('/rol/index', 'RolController@index')->name('rol.index');
Route::get('/rol/crear', 'RolController@create')->name('rol.create');
Route::post('/rol/guardar', 'RolController@store')->name('rol.store');
Route::get('/rol/ver/{id}', 'RolController@show')->name('rol.show');
Route::get('/rol/editar/{id}', 'RolController@edit')->name('rol.edit');
Route::put('/rol/editar/update/{id}', 'RolController@update')->name('rol.update');
Route::delete('/rol/eliminar/{id}', 'RolController@destroy')->name('rol.destroy');

Route::get('/usuario/index', 'UserController@index')->name('usuario.index');
Route::get('/usuario/ver/{id}', 'UserController@show')->name('usuario.show');
Route::get('/usuario/editar/{id}', 'UserController@editarUser')->name('usuario.edit');
Route::put('/usuario/editar/update/{id}', 'UserController@update')->name('usuario.update');

Route::get('/usuario/crear', 'UserController@crearUser')->name('usuario.create');
Route::post('/usuario/guardar', 'UserController@store')->name('usuario.store');
Route::delete('/usuario/eliminar/{id}', 'UserController@eliminarUser')->name('usuario.destroy');

Route::post('/areas/importar', 'ExcelController@importarAreas');
Route::post('/profesionales/importar', 'ExcelController@importarProfesionales');
Route::post('/proyectos/importar', 'ExcelController@importarProyectos');

Route::get('/areas/{id}/editar/subareas/crear', 'AreaController@createSubarea')->name('subareas.create');
Route::post('/areas/{id}/editar/subareas/guardar', 'AreaController@storeSubarea')->name('subareas.store');
Route::get('/areas/{id}/editar/subareas/{idSub}/editar', 'AreaController@editSubarea')->name('subareas.edit');
Route::put('/areas/{id}/editar/subareas/{idSub}', 'AreaController@updateSubarea')->name('subareas.update');
Route::delete('/areas/{id}/editar/subareas/{idSub}', 'AreaController@destroySubarea')->name('subareas.destroy');
/*COMPETENCIAS Y INTERES*/
Route::get('/competencias', 'UserController@competenciasMostrar');
Route::get('are/{id}', 'UserController@getArea');
Route::post('guardar', 'UserController@competenciasGuardar')->name('guardar.competencias');

Route::get('/interes', 'UserController@interesMostrar');
Route::post('guardarinteres', 'UserController@interesGuardar')->name('guardar.interes');
/*USER*/
Route::get('/User/password', 'UserController@password');
Route::post('/User/updatepassword', 'UserController@updatePassword');
Route::get('/secretaria/perfil','UserController@getProfileSecretaria')->name('secretaria.perfil');
Route::put('/secretaria/perfil/update/','UserController@updateProfileSecretaria')->name('secretaria.perfil.update');
Route::post('/secretaria/update/foto','UserController@updateFotoSecretaria')->name('secretaria.update.foto');


Route::get('/admin/perfil','UserController@getProfileAdministrador')->name('admin.perfil');
Route::put('/admin/perfil/update/','UserController@updateProfileAdministrador')->name('admin.perfil.update');
Route::post('/admin/update/foto','UserController@updateFotoAdministrador')->name('admin.update.foto');

Route::get('consejero/perfil','UserController@getProfileConsejero')->name('consejero.perfil');
Route::put('/consejero/perfil/update','UserController@updateProfileConsejero')->name('consejero.perfil.update');
Route::post('/consejero/update/foto','UserController@updateFotoConsejero')->name('consejero.update.foto');

Route::get('/perfil','ProfesionalController@showProfesional')->name('perfil.show');
Route::put('/perfil/{id}','ProfesionalController@updateProfesional')->name('perfil.update');
Route::post('/perfil/update/foto','ProfesionalController@updateFotoProfesional')->name('perfil.update.foto');
Route::get('/profesional/proyecto', 'ProfesionalController@getProyectos')->name('profesional.proyecto');

Route::get('/asignaciones', 'AsignacionController@index')->name('asignacion.index');
Route::get('/asignaciones/{id}/asignar', 'AsignacionController@asignarTribunales')->name('asignaciones.asignar');
Route::get('/asignaciones/{id}/profesionales/{idProfesional}/reasignar', 'AsignacionController@reasignarTribunal')->name('asignaciones.reasignar');
Route::get('/asignaciones/{id}/tribunales', 'AsignacionController@getTribunales')->name('asignaciones.tribunales');
Route::get('/asignaciones/{id}/estado', 'AsignacionController@ProyectoDefendido')->name('asignaciones.defendido');
Route::get('/asignaciones/proyectosAsignados', 'AsignacionController@getProyectosAsignados')->name('asignaciones.asignados');
Route::post('/asignaciones/{id}/notificar', 'AsignacionController@notificarTribunales')->name('asignaciones.notificar');
Route::post('/asignaciones/{id}/profesionales/{idProfesional}/notificarReasignacion', 'AsignacionController@notificarTribunalReasignado')->name('asignaciones.notificarReasignacion');

Route::resources([
    'reglas' => 'ReglaController',
    'areas' => 'AreaController',
    'profesionales' => 'ProfesionalController',
    'proyectos' => 'ProyectoController'
]);
