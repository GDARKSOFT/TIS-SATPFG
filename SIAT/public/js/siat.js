$(document).ready(function () {
  $('#iconEyeActual').click(function () {
    if ($('#mypassword').attr('type') == 'text') {
      $('#mypassword').attr('type', 'password');

    } else {
      $('#mypassword').attr('type', 'text');
    }
    $('#iconActual').toggleClass('fa-eye-slash').toggleClass('fa-eye');
  });
});
//
$(document).ready(function () {
  $('#iconEye').click(function () {
    if ($('#password').attr('type') == 'text') {
      $('#password').attr('type', 'password');

    } else {
      $('#password').attr('type', 'text');
    }
    $('#icon').toggleClass('fa-eye-slash').toggleClass('fa-eye');
  });
});
//
$(document).ready(function () {
  $('#iconEyeConfirm').click(function () {
    if ($('#password-confirm').attr('type') == 'text') {
      $('#password-confirm').attr('type', 'password');

    } else {
      $('#password-confirm').attr('type', 'text');
    }
    $('#iconConfirm').toggleClass('fa-eye-slash').toggleClass('fa-eye');
  });
});

$('div.alert').not('.alert-important').delay(4000).fadeOut(350);

/*Modal*/
$(document).on('click', '.delete-modal',function (e) {
  $('.delete-form').attr('action', $(this).data('delete-link'));
  $('.modal-body').text($(this).data('body'));
});

/*Datatable*/
var esp = {
  sProcessing:     "Procesando...",
  sLengthMenu:     "Mostrar _MENU_ registros",
  sZeroRecords:    "No se encontraron resultados",
  sEmptyTable:     "Ningún dato disponible en esta tabla",
  sInfo:           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
  sInfoEmpty:      "Mostrando registros del 0 al 0 de un total de 0 registros",
  sInfoFiltered:   "(filtrado de un total de _MAX_ registros)",
  sInfoPostFix:    "",
  sSearch:         "Buscar:",
  sUrl:            "",
  sInfoThousands:  ",",
  sLoadingRecords: "Cargando...",
  oPaginate: {
    sFirst:    "Primero",
    sLast:     "Último",
    sNext:     "Siguiente",
    sPrevious: "Anterior"
  },
  oAria: {
    sSortAscending:  ": Activar para ordenar la columna de manera ascendente",
    sSortDescending: ": Activar para ordenar la columna de manera descendente"
  }
};

$.fn.dataTableExt.ofnSearch['string'] = function ( data ) {
  return ! data ?
      '' :
      typeof data === 'string' ?
          data
              .replace( /\n/g, ' ' )
              .replace( /á/g, 'a' )
              .replace( /é/g, 'e' )
              .replace( /í/g, 'i' )
              .replace( /ó/g, 'o' )
              .replace( /ú/g, 'u' )
              .replace( /ê/g, 'e' )
              .replace( /î/g, 'i' )
              .replace( /ô/g, 'o' )
              .replace( /è/g, 'e' )
              .replace( /ï/g, 'i' )
              .replace( /ü/g, 'u' )
              .replace( /ç/g, 'c' ) :
          data;
  };
