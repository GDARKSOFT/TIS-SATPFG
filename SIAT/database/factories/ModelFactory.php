<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Regla::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id_reg' => $faker->increments(),
        'des_reg' => $faker->text(140),
        'val_reg' => $faker->integer(),
    ];
});
$factory->define(App\Profesional::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'id_prof' => $faker->increments(),
        'ci_prof' => $faker->integer(10),
        'nom_prof'=> $faker->text(30),
        'app_prof' => $faker->text(40),
        'apm_prof' => $faker->text(40),
        'tel_prof' => $faker->text(8),
        'dir_prof' => $faker->text(100),
        'cor_prof' => $faker->text(30),
    ];
});