<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('carr_id')->unsigned();
            $table->foreign('carr_id')->references('id')->on('carrera')->onDelete('cascade');
            $table->string('tit_proy',1000);
            $table->string('nom_est');
            $table->string('app_est');
            $table->string('apm_est');
            $table->integer('tut_id')->unsigned();
            $table->foreign('tut_id')->references('id_prof')->on('profesional')->onDelete('cascade');
            $table->integer('mod_id')->unsigned();
            $table->foreign('mod_id')->references('id')->on('modalidad')->onDelete('cascade');
            $table->integer('est_id')->unsigned();
            $table->foreign('est_id')->references('id')->on('estado')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto');
    }
}
