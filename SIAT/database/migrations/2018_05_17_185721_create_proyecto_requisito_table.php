<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProyectoRequisitoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('proyecto_requisito', function (Blueprint $table) {
            $table->integer('proy_id')->unsigned();
            $table->foreign('proy_id')->references('id')->on('proyecto');
            $table->integer('req_id')->unsigned();
            $table->foreign('req_id')->references('id')->on('requisito');
            $table->boolean('entregado')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('proyecto_requisito');
    }
}
