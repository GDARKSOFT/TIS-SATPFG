<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ci_user', 100)->nullable();
            $table->string('nom_user', 100);
            $table->string('app_user',100)->nullable();
            $table->string('apm_user',100)->nullable();
            $table->string('email', 100)->unique();
            $table->string('avatar')->default('default.jpg');
            $table->text('password', 100);
            $table->integer('prof_id');
            $table->string('tel_user',100)->nullable();
            $table->string('dir_user',100)->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
