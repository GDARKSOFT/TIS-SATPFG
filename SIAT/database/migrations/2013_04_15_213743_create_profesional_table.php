<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfesionalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profesional', function (Blueprint $table) {
            $table->increments('id_prof')->nullable();
            $table->string('ci_prof', 50)->nullable()->default(0);
            $table->integer('cod_prof')->nullable();
            $table->string('nom_prof', 100)->nullable();
            $table->string('app_prof', 100)->nullable();
            $table->string('apm_prof', 100)->nullable();
            $table->integer('tel_prof')->nullable();
            $table->string('dir_prof', 100)->nullable();
            $table->string('cor_prof', 100)->unique();
            $table->string('trab_prof')->default('Interno');
            $table->string('avatar')->default('default.jpg');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profesional');
    }
}
