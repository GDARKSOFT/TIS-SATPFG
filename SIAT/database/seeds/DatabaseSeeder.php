<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FuncionSeeder::class);
        $this->call(RolSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(CarreraSeeder::class);
        $this->call(RequisitoSeeder::class);
        $this->call(ModalidadSeeder::class);
        $this->call(EstadoSeeder::class);

        DB::table('regla')->insert([
            'des_reg' => 'Cantidad de proyectos máximo que puede tener asignado un docente o profesional',
            'val_reg' => 4,
        ]);
    }

}
