<?php

use Illuminate\Database\Seeder;
use App\Funcion;

class RolSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $fun_1 = Funcion::where('des_fun', 'Home')->first();
        $fun_2 = Funcion::where('des_fun', 'Usuarios')->first();
        $fun_3 = Funcion::where('des_fun', 'Roles')->first();
        $fun_4 = Funcion::where('des_fun', 'Reglas')->first();
        $fun_5 = Funcion::where('des_fun', 'Areas')->first();
        $fun_6 = Funcion::where('des_fun', 'Profesionales')->first();
        $fun_7 = Funcion::where('des_fun', 'Proyectos')->first();
        $fun_8 = Funcion::where('des_fun', 'Proyectos Asignados')->first();
        $fun_9 = Funcion::where('des_fun', 'Proyectos Habilitados')->first();
        $fun_10 = Funcion::where('des_fun', 'Proyectos a Evaluar')->first();
        $fun_11 = Funcion::where('des_fun', 'Competencias')->first();
        $fun_12 = Funcion::where('des_fun', 'Intereses')->first();
       
        $rol = new \App\Rol();
        $rol->des_rol = 'Administrador';
        $rol->save();
        $rol->funciones()->attach($fun_1->id);
        $rol->funciones()->attach($fun_2->id);
        $rol->funciones()->attach($fun_3->id);

        $rol = new \App\Rol();
        $rol->des_rol = 'Secretaria';
        $rol->save();
        $rol->funciones()->attach($fun_1->id);
        $rol->funciones()->attach($fun_4->id);
        $rol->funciones()->attach($fun_5->id);
        $rol->funciones()->attach($fun_6->id);
        $rol->funciones()->attach($fun_7->id);


        $rol = new \App\Rol();
        $rol->des_rol = 'Consejero';
        $rol->save();
        $rol->funciones()->attach($fun_1->id);
        $rol->funciones()->attach($fun_4->id);
        $rol->funciones()->attach($fun_8->id);
        $rol->funciones()->attach($fun_9->id);

        $rol = new \App\Rol();
        $rol->des_rol = 'Profesional';
        $rol->save();
        $rol->funciones()->attach($fun_1->id);
        $rol->funciones()->attach($fun_10->id);
        $rol->funciones()->attach($fun_11->id);
        $rol->funciones()->attach($fun_12->id);

    }
}
