<?php

use Illuminate\Database\Seeder;

class FuncionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Home';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Profesionales';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Usuarios';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Roles';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Reglas';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Areas';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Proyectos';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Proyectos Asignados';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Proyectos Habilitados';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Competencias';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Intereses';
        $funcion->save();

        $funcion = new \App\Funcion();
        $funcion->des_fun = 'Proyectos a Evaluar';
        $funcion->save();
    }
}
