<?php

use Illuminate\Database\Seeder;
use App\Rol;
use App\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol_administrador = Rol::where('des_rol', 'Administrador')->first();
        $rol_secretaria = Rol::where('des_rol', 'Secretaria')->first();
        $rol_consejero = Rol::where('des_rol', 'Consejero')->first();
        $rol_profesional = Rol::where('des_rol', 'Profesional')->first();
        
        $user = new User();
        $user->nom_user = 'Admin';
        $user->email = 'siat.umss@gmail.com';
        $user->password = bcrypt('Admin-123');
        $user->prof_id = 0;
        $user->save();
        $user->roles()->attach($rol_administrador->id);
    }
}
