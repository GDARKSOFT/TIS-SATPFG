<?php

use Illuminate\Database\Seeder;

class EstadoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rol = new \App\Estado;
        $rol->des_est = 'Deshabilitado';
        $rol->save();

        $rol = new \App\Estado;
        $rol->des_est = 'Habilitado';
        $rol->save();

        $rol = new \App\Estado;
        $rol->des_est = 'Asignado';
        $rol->save();

        $rol = new \App\Estado;
        $rol->des_est = 'Defendido';
        $rol->save();
    }
}
