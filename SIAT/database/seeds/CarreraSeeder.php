<?php

use Illuminate\Database\Seeder;
use App\Carrera;

class CarreraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $carrera = new Carrera;
        $carrera->des_carr = 'Licenciatura en Ingenieria Informática';
        $carrera->save();

        $carrera = new Carrera;
        $carrera->des_carr = 'Licenciatura en Ingenieria de Sistemas';
        $carrera->save();
    }
}
