<?php

use Illuminate\Database\Seeder;
use App\Modalidad;
use App\Requisito;

class ModalidadSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $req_a = Requisito::where('des_req', 'Carta del estudiante dirigida al H. Consejo de Carrera solicitando la designacion o nombramiento de los tribunales para la revision de su trabajo de grado, indicando el nombre de su trabajo de grado y la modalidad de titulación.')->first();
        $req_b = Requisito::where('des_req', 'Carta del tutor dirigida al H. Consejo de carrera, indicando que el trabajo de grado esta concluido y apto para la revision por el tribunal.')->first();
        $req_c = Requisito::where('des_req', 'Carta del docente de materia de modalidad de titulacion dirigida al H.  Consejo de carrera, indicando que ell trabajo de grado ya esta apto para la revision por el tribunal.')->first();
        $req_d = Requisito::where('des_req', 'Tres copias del documento del trabajo de grado (anillados), de acuerdo a la guia para la elaboracion de trabajo de grado.')->first();
        $req_e = Requisito::where('des_req', 'Para los trabajos de grado bajo modalidad de titulacion de trabajo dirigido o adscripcion, una carta del responsable de la unidad, empresa o institucion solicitante, indicando que el trabajo de grado ha sido concluido satisfactoriamente de acuerdo a los objetivos y alcances previstos en el convenio.')->first();

        $modalidad = new Modalidad;
        $modalidad->des_mod = 'Tesis';
        $modalidad->save();
        $modalidad->requisitos()->attach($req_a->id);
        $modalidad->requisitos()->attach($req_b->id);
        $modalidad->requisitos()->attach($req_c->id);
        $modalidad->requisitos()->attach($req_d->id);

        $modalidad = new Modalidad;
        $modalidad->des_mod = 'Proyecto de Grado';
        $modalidad->save();
        $modalidad->requisitos()->attach($req_a->id);
        $modalidad->requisitos()->attach($req_b->id);
        $modalidad->requisitos()->attach($req_c->id);
        $modalidad->requisitos()->attach($req_d->id);

        $modalidad = new Modalidad;
        $modalidad->des_mod = 'Trabajo Dirigido';
        $modalidad->save();
        $modalidad->requisitos()->attach($req_a->id);
        $modalidad->requisitos()->attach($req_b->id);
        $modalidad->requisitos()->attach($req_c->id);
        $modalidad->requisitos()->attach($req_d->id);
        $modalidad->requisitos()->attach($req_e->id);

        $modalidad = new Modalidad;
        $modalidad->des_mod = 'Adscripción';
        $modalidad->save();
        $modalidad->requisitos()->attach($req_a->id);
        $modalidad->requisitos()->attach($req_b->id);
        $modalidad->requisitos()->attach($req_c->id);
        $modalidad->requisitos()->attach($req_d->id);
        $modalidad->requisitos()->attach($req_e->id);
    }
}
